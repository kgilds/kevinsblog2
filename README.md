
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Kevin’s Blog

<!-- badges: start -->

[![Netlify
Status](https://api.netlify.com/api/v1/badges/a6f73ca2-621e-4ac9-94d8-7b5814540b15/deploy-status)](https://app.netlify.com/sites/laughing-babbage-4b6083/deploys)
<!-- badges: end -->

# Stats

Thank you to
[Matt-Dray](https://github.com/matt-dray/rostrum-blog/blob/master/README.Rmd)
and [Shannon
Pileggi](https://github.com/shannonpileggi/pipinghotdata_distill/blob/master/README.Rmd)
for demonstrating how to scrape your own website to grab some stats.
Below is what I could manage. I learned a ton doing updating this
README.

There have been 1 posts on [the blog](https://kgilds.rbind.io/post/)
since 2019-03-27.

The latest post was published 1209 day(s) ago:
[Pets](https://kgilds.rbind.io/post/2019-03-27-pets/pets/).

|   n | publish_date | Title                                                      |
|----:|:-------------|:-----------------------------------------------------------|
|  14 | 2019-03-27   | [Pets](https://kgilds.rbind.io/post/2019-03-27-pets/pets/) |

*Updated 2022-07-18 06:39:20*
