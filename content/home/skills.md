+++
# Feature/Skill widget.
widget = "featurette"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Skills"
subtitle = ""

# Order that this section will appear in.
weight = 30

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

[[feature]]
  icon = "r-project"
  icon_pack = "fab"
  name = "R"
  description = ""
  
[[feature]]
  icon = "database" 
  icon_pack = "fa"
  name = "SQL"

  
[[feature]]
  icon = "heart"
  icon_pack = "fa"
  name = "Nonprofit Organizations"
  description = "Expertise in Collecting, Managing and Reporting Nonprofit Data"
  
  [[feature]]
  icon = "github" 
  icon_pack = "fab"
  name = "Git"
  
  
  [[feature]]
  icon = "chart-line"
  icon_pack = "fas"
  name = "Data Visualization"
  description = ""  
  
[[feature]]
  icon = "table"
  icon_pack = "fas"
  name = "Data Wrangling"
  description = ""


  

+++
