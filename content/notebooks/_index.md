+++
title = "Notebooks"

date = 2018-09-09T00:00:00
# lastmod = 2018-09-09T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

# Add menu entry to sidebar.
[menu.notebooks]
  name = "Notebooks"
  weight = 1
+++

A notebook gives you the freedom to openly think about topics. Below are some notebooks I am working on.
