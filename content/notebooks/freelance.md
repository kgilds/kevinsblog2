+++
title = "Lessons"

date = 2018-09-09T00:00:00
# lastmod = 2018-09-09T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

# Add menu entry to sidebar.
linktitle = "Lessons"
[menu.notebooks]
  parent = "Freelance Workshop"
  weight = 1
+++

The purpose of the Freelance Workshop is help one enter into  a virtious cycle of obtaining more respect and earning better clients. The lessons consists of writing prompts and writing and receiving feedback from others. 



## Lesson 0

...

## Lesson 1

...
