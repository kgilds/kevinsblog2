+++
# Project title.
title = "Florida School Absences"

# Date this page was created.
date = 2019-03-09T00:00:00

# Project summary to display on homepage.
summary = "Shiny Application that parses a Florida Department of Education spreadsheet containing counts and percent of students with 21 or more days with absences. This shiny application lets you segment by school district and type of school `external_link`."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Shiny Applications", "Education-Data"]

# Optional external URL for project (replaces project detail page).
external_link = "https://tidydatabykwg57.shinyapps.io/flabsences/"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = "Photo by Toa Heftiba on Unsplash"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++
