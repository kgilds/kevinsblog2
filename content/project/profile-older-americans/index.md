+++
# Project title.
title = "Profile of Older Americans"

# Date this page was created.
date = 2020-05-15T00:00:00


# Project summary to display on homepage.
summary = "The goal of this project was to build a Shiny Application that highlights data from Profile of Older Americans published by the Administration for Community Living and to submit to the Rstudio Shiny Contest. `external_link`."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Shiny Applications", "Shiny Contest", "ACL"]

# Optional external URL for project (replaces project detail page).
external_link = "https://tidydatabykwg57.shinyapps.io/ACLOlderAmericansProfile/"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = "Map of the USA"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++