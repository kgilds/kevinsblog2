+++
# Project title.
title = "Chromebook Data Science"

# Date this page was created.
date = 2019-03-30T00:00:00

# Project summary to display on homepage.
summary = "Kevin's progress through the Chromebook Data Science Course Sets."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Chromebook Data Science"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = "#"

# Links (optional).
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"

url_slides = ""
url_video = ""
url_code = ""




# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{icon_pack = "fab", icon="twitter", name="Follow", url = "https://twitter.com/kevin_gilds"},
{name="Introduction to Chromebook Data Science", url = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"},
{name= "How to use a Chromebook", url = "https://drive.google.com/file/d/1gW0ptvvhQPtmARnEBWl2DHPjRUrg8AMQ/view?usp=sharing"},
{name ="Google and the Cloud", url = "https://drive.google.com/file/d/1SIQUNa_b37VA_HFG0iuX9MTsP2zd0jCF/view?usp=sharing"},
{name ="Organizing Data Science Projects", url = "https://drive.google.com/file/d/1qy38DLzwtZ0eypWiZt6YGAqrpWMY9AIr/view?usp=sharing]"},
{name = "Version Control", url = "https://drive.google.com/file/d/1o7rQRuEydmAukUvriTXLR3oJZIixfaKy/view?usp=sharing"},
{name = "Data Tidying", url = "https://drive.google.com/file/d/12kM3QtOk9qloZFvB39O9pXawPMuubsKn/view?usp=sharing"},
{name = "Data Visualization", url = "https://drive.google.com/file/d/1kQKtEsKJza0xKm9qEdBLPdbTARhK6L0I/view?usp=sharing"},
{name = "Getting Data", url = "https://drive.google.com/file/d/1d9B2xkHsQ0o5ipLIRv7YWOPifbrdsNn7/view?usp=sharing"}]

 

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = "Kevin's Chromebook"
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++

### Posts

[Learning Posts](https://kgilds.rbind.io/tags/chromebook-data-science/) a page that highlights key insights from the Chromebook Data Science Courses. 
