+++
# Project title.
title = "The Freelancer's Workshop"

# Add menu entry to sidebar.
[menu.tutorial]
  name = "Lessons"
  weight = 1  


# Date this page was created.
date = 2020-05-22T00:00:00

# Project summary to display on homepage.
summary = "Kevin's progress through the Freelancer's Workshop."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Freelancer's Workshop"]

# Optional external URL for project (replaces project detail page).
external_link = ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = "#"

# Links (optional).
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"

url_slides = ""
url_video = ""
url_code = ""




# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{icon_pack = "fab", icon="twitter", name="Follow", url = "https://twitter.com/kevin_gilds"}]

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
  

  
+++





Here is the nice [Certificate](freelance.pdf)





