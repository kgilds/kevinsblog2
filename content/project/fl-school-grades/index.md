+++
# Project title.
title = "Florida School Grades"

# Date this page was created.
date = 2020-03-28T00:00:00

# Project summary to display on homepage.
summary = "Shiny Application that parses a Florida Department of Education spreadsheets containing District and School Grades . This shiny application lets you segment by school district and type of school `external_link`."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Shiny Applications", "Education-Data"]

# Optional external URL for project (replaces project detail page).
external_link = "https://tidydatabykwg57.shinyapps.io/florida_school_grades/"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = "Photo by Toa Heftiba on Unsplash"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++