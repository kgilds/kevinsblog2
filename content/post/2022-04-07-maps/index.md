---
title: Maps
author: Kevin
date: '2022-04-07'
slug: []
categories:
  - R
tags:
  - Maps
image:
  caption: ''
  focal_point: ''
---


I want to check this out <blockquote class="twitter-tweet"><p lang="en" dir="ltr">If you missed <a href="https://twitter.com/SeriFeliciano?ref_src=twsrc%5Etfw">@SeriFeliciano</a> teaching about Getting Started with Spatial Data yesterday, you are in luck! We have a video of the event on our YouTube channel... Thanks again <a href="https://twitter.com/SeriFeliciano?ref_src=twsrc%5Etfw">@SeriFeliciano</a> for a really useful presentation that made mapping in R do-able! 💜 <a href="https://t.co/lOowmzYrOE">https://t.co/lOowmzYrOE</a></p>&mdash; R-LadiesSTL (@RLadiesSTL) <a href="https://twitter.com/RLadiesSTL/status/1512182759439093767?ref_src=twsrc%5Etfw">April 7, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 