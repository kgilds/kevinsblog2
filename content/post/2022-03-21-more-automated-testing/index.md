---
title: More Automated Testing
author: Kevin
date: '2022-03-21'
slug: []
categories:
  - R
tags:
  - gitlab
image:
  caption: ''
  focal_point: ''
---
 
 
My saga continues with trying to render `covrpage` report in my gitlab ci/cd yaml file. 

I tried to cp it into the public folder and then I tried to just name the file in the artifact call. No luck--it is time to leave it alone for a bit. 