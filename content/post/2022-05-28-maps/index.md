---
title: Maps
author: Kevin
date: '2022-05-28'
slug: []
categories:
  - R
tags:
  - maps
  - tidycensus
  - ggplot2
---


The label section of this code took me forever to figure out. All the stack overflow posts had `labels = comma` 




```
scale_fill_viridis_c(option = "viridis", direction = 1, labels = label_dollar()) +

```


I discovered the solution here: https://walker-data.com/census-r/mapping-census-data-with-r.html

I wish to try some of the other mapping packages. 
