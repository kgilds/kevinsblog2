---
title: Page Down and R-Markdown Reports with Parameters
author: Kevin
date: '2019-01-26'
slug: page-down-and-r-markdown-reports-with-parameters
categories:
  - R
tags:
  - Rmarkdown
header:
  caption: ''
  image: ''
---

The RStudio Conference Videos are up [here](https://resources.rstudio.com/rstudio-conf-2019). Two videos that jumped at me tonight are the [Pagedown Package](https://resources.rstudio.com/rstudio-conf-2019/pagedown-creating-beautiful-pdfs-with-r-markdown-and-css?prevItm=0&prevCol=3563601&ts=299851) and the [Parameterised Reports](https://resources.rstudio.com/rstudio-conf-2019/the-lazy-and-easily-distracted-report-writer-using-rmarkdown-and-parameterised-reports).

Hopefully, I find the Shine Module video shortly. 



