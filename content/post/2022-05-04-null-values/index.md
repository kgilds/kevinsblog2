---
title: NULL Values
author: Kevin
date: '2022-05-04'
slug: []
categories:
  - R
tags:
  - SQL
image:
  caption: ''
  focal_point: ''
---

What I love about the book _SQL for Data Scientists_ is one can tell it is written by someone who does the work. I can tell there is a whole section on NULL values and tips on finding blank values in a data set. 

```
WHERE 
   product_size IS NULL
OR TRIM(product_size) = ''

```