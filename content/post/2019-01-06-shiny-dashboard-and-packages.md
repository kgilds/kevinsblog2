---
title: Shiny Dashboard and Packages
author: Kevin
date: '2019-01-06'
slug: shiny-dashboard-and-packages
categories:
  - R
tags:
  - Work
header:
  caption: ''
  image: ''
---

Today, I became keenly aware that you can't expect to upload a Shiny App to Shiny App.io if one is using an internal package. The package has to either come from CRAN or Github.  This makes a lot of sense in retrospect.


My workaround was wrap the needed function in the server section of the Shiny Application. 



