---
title: RPubs
author: Kevin
date: '2022-02-26'
slug: []
categories:
  - R
tags:
  - Rmarkdown
image:
  caption: ''
  focal_point: ''
---

I wanted to publish an Rmarkdown Document to RPubs and had to look some items up. Well, I found old posts that I had forgotten about. 

I worked hard on this [one](https://rpubs.com/kgilds/DataVizChallenge2) and this was before git entered my life so that code was gone. 

This [post](https://rpubs.com/kgilds/306277) was worked on while Hurricane Irma was bearing down on Florida. 