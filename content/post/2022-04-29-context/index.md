---
title: Context
author: Kevin
date: '2022-04-29'
slug: []
categories:
  - R
tags:
  - data management
image:
  caption: ''
  focal_point: ''
---


This is a good [article](https://counting.substack.com/p/data-management-is-context-management?s=w) about the importance of context in data work.

Here is a hell of quote from the article. 

> The act of counting and grouping is, by necessity, context destroying.