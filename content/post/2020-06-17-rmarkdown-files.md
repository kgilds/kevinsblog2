---
title: Weekly Links
author: kevin
date: '2020-06-17'
slug: rmarkdown-files
categories:
  - craftsmanship
tags: [books, quokka, writing]
image:
  caption: ''
  focal_point: ''
---

Here is an excellent [post](https://drmowinckels.io/blog/2020-05-25-changing-you-blogdown-workflow/) on a Blogdown workflow


More bloddown resources:

[Archetypes](http://lcolladotor.github.io/2018/03/08/blogdown-archetype-template/#.XuuSnEXYrrc)
[Page Bundles](https://alison.rbind.io/post/2019-02-21-hugo-page-bundles/)


This [website](https://sivers.org/book) on book notes was recommended in the Quokka Mastermind group. Tooling around the site this [article](https://sivers.org/nod) This has been my frustration with the practice of trying to publish everyday--

Check out these [notes](https://sivers.org/book/OnWritingWell) on writing well. 

> The essence of writing is rewriting. 

> Ask what I am trying to say and then ask have I said it

Maybe you need to write enough that there is enough to edit so you have something to push? 