---
title: Troubleshooting
author: Kevin
date: '2022-02-09'
slug: []
categories:
  - R
tags:
  - qualtRics
image:
  caption: ''
  focal_point: ''
---

This morning, I updated a function that is a wrapper around the 'fetch_survey' function in the  [qualtRics package](https://docs.ropensci.org/qualtRics/index.html) 

I received an error that I needed to set up my qualtrics api credentials.  

Okay, lets start with the most simple solution. 


> After you use this function, reload your environment (readRenviron("~/.Renviron")) so you can use the credentials without restarting R.

I skipped this function `qualtrics_api_credentials` because I had already completed this step and just ran the following script. 

```
readRenviorn("~/.Renviron")

```

And it worked. 
