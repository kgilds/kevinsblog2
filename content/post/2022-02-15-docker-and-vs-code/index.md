---
title: Docker and VSCode
author: Kevin
date: '2022-02-15'
slug: []
categories:
  - Docker
tags: [Docker]
image:
  caption: ''
  focal_point: ''
---

I thought about implementing a docker based workflow through `vscode` but alas fell on the safe side. 

https://youtu.be/4wRiPG9LM3o

Good thing, I can't get my terminal in VS code to recognize docker.

It is something to strive for. 