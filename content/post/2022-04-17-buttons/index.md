---
title: Buttons
author: Kevin
date: '2022-04-17'
slug: []
categories:
  - R
tags:
  - Shiny
image:
  caption: ''
  focal_point: ''
---


Today, I learned how to format an action button in Shiny. I must have glazed over this in previous readings but it is right there in [Mastering Shiny](https://mastering-shiny.org/basic-ui.html#action-buttons)

```
ui <- fluidPage(
  fluidRow(
    actionButton("click", "Click me!", class = "btn-danger"),
    actionButton("drink", "Drink me!", class = "btn-lg btn-success")
  ),
  fluidRow(
    actionButton("eat", "Eat me!", class = "btn-block")
  )
)

```

The content links [here](https://bootstrapdocs.com/v3.3.6/docs/css/#buttons) as well for more information. I added the `active` argument and that filled in the button with the color of the `btn-success` status--green.  