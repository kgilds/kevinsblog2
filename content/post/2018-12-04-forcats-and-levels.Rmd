---
title: forcats and levels
author: Kevin
date: '2018-12-04'
slug: forcats-and-levels
categories:
  - R
tags:
  - chromebook data science
  - WorkNotes
header:
  caption: ''
  image: ''
---

Did I know this?  I am not sure but it was helpful to learn again. 

The key is making the all_days vector and using that to establish the levels for the factor. I don't think I was aware that you could create the level and not have it in the answer choices. 

```{r}
library(forcats)
```



```{r}
all_days <- c("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
```


```{r}
some_days <- c("Friday", "Saturday", "Sunday")
```


```{r}
days <- factor(some_days, levels = all_days)

```

