---
title: See Differences
author: Kevin
date: '2022-03-12'
slug: []
categories:
  - R
tags: [Waldo, Janitor]
image:
  caption: ''
  focal_point: ''
---

The battles against duplicate values continues, and this morning I was racking my brain trying to find a workflow to segment a data frame in order to determine if there is a difference. Sometimes, it is truly a duplicate value and someone entered a record twice. This data set happens to have 58 columns and wanted more than an eyeball test.  


It turns out I already had a decent solution. and here is what I came up with.  


```r
gr_dupes <- janitor::get_dupes(survey, student_code, site)



id <- gr_dupes %>%
  dplyr::filter(student_code == "321ZZ0175") 



a <- id %>%
  dplyr::slice(1)


b <- id %>%
  dplyr::slice(2)



test <- waldo::compare(a, b, max_diffs = 21)  

```

The output of the differences from the compare function only displayed 10 differences but told me there were 11 more. I figured out that the
The `max_diffs` argument defaults to 10, and I needed to increased the default to see more differences.  