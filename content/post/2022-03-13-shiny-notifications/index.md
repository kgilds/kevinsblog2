---
title: Shiny Notifications
author: Kevin
date: '2022-03-13'
slug: []
categories:
  - R
tags: [Shiny, shinyjs]
image:
  caption: ''
  focal_point: ''
---

This morning I was working with download buttons in one of my Shiny Application and realized it takes some time for the Rmarkdown report to render. I want to add a notification for the user.  I reviewed some examples in [_Mastering Shiny_](https://mastering-shiny.org/action-feedback.html) and realized nothing seemed to fit this use case. 

I completed a search and found this [stack overflow post](https://stackoverflow.com/questions/63243929/alert-notification-in-shiny) and realized this was a bit more complicated than I imagined. However very approachable with the [`shinyjs`](https://deanattali.com/shinyjs/) package. 