---
title: Building a moat
author: Kevin
date: '2018-12-18'
slug: building-a-moat
categories:
  - podcast
tags:
  - podcast
header:
  caption: ''
  image: ''
---

This episode was on Recode/Decode. Sure seems like [Sweetgreen](https://www.sweetgreen.com/) is trying they are trying to build their own moat. This interview left me impressed with the co-founder Johnathon Newman and wanting a salad. 


<iframe src="https://art19.com/shows/recode-decode/episodes/38758ee8-83f8-4a6d-af63-463614b54549/embed?theme=dark-custom" style="width: 100%; height: 200px; border: 0 none;" scrolling="no"></iframe>
