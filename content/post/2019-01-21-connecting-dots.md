---
title: Connecting Dots
author: Kevin
date: '2019-01-21'
slug: connecting-dots
categories:
  - management
tags:
  - Books
header:
  caption: ''
  image: ''
---

This morning, I finished [*The Innovators*](https://www.simonandschuster.com/books/The-Innovators/Walter-Isaacson/9781476708706) by Walter Isaacson. A very enjoyable and informative book. I loved the last chapter where Isaacson talks about the importance of connecting art and science. Programmers would benefit from understanding art and a poet could benefit from leaning to code. Can you say Fackbook--Kara Swisher criticism of Mark Zuckerberg. 

Don't collect dots--connect them is the advice. 

