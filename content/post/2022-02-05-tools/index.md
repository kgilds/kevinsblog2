---
title: Tools
author: Kevin
date: '2022-02-05'
slug: []
categories:
  - R
tags: [WIC]
image:
  caption: ''
  focal_point: ''
---

A big challenge with starting programming can be the install process of the tools. 

Below are resources to help a person test drive a coding approach. 

* [RStudio](https://www.rstudio.com/products/cloud/)
	* Mainly `R` but one can use other languages such as 'Python' and 'SQL'  

* [Google Colab](https://colab.research.google.com/)
	* Mainly `Python` but on can use `R` as well. 

* [Kaggle](https://www.kaggle.com/)