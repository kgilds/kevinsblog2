---
title: Counting can be hard
author: Kevin
date: '2022-02-08'
slug: []
categories:
  - WIC
tags:
  - management
image:
  caption: ''
  focal_point: ''
---

Here is a nice post on [systems](https://counting.substack.com/p/reflecting-on-how-system-complexity?utm_campaign=post&utm_medium=email) on systems and data analysis. 


This is a powerful quote from the article about departments being fragmented. 

> Everyone builds tools and models of their world that track what they care about, to the exclusion of everything else.


The idea, I think, is to build an over-arching system that can incorporate all the sub systems?