---
title: Package Versions
author: Kevin
date: '2022-05-30'
slug: []
categories:
  - R
tags:
  - waffle
image:
  caption: ''
  focal_point: ''
---


I could not get a function to work from the waffle package and had the hardest time figuring out why. I was not using the most updated package.  I have had this issue come up a couple of times now I know where to look on Github. I suspect I initially pulled it down from CRAN and the version I wanted was on `master/main` branch Github. Once I confirmed it was a simple call to this 

```r
devtools::install_github("hrbrmstr/waffle") 

```