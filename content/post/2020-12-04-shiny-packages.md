---
title: 'Shiny Packages '
author: Kevin
date: '2020-12-04'
slug: shiny-packages
categories:
  - R
tags: [shiny]
image:
  caption: ''
  focal_point: ''
---

I was catching up on the latest release of the [Shiny Developer Series](https://shinydevseries.com/post/episode-16-jmclellan/) with Jiena Gu McLellan

She has some packages that seem fun to try.


1.   [faq](https://github.com/jienagu/faq)

2.   [noteMd](https://github.com/jienagu/noteMD)

3.   [DT-Editor](https://github.com/jienagu/DT-Editor)

4.   [flashCard](https://github.com/jienagu/flashCard)



And, reviewing the series it reminded me to try this package again.

 [fullPage](https://fullpage.rinterface.com/index.html)
