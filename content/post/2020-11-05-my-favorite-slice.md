---
title: My favorite slice
author: Kein
date: '2020-11-05'
slug: my-favorite-slice
categories:
  - R
tags:
  - dplyr
  - slice
image:
  caption: ''
  focal_point: ''
---

I am been working with building tests and needed to find a consistent way to grab a value in the last row. Of course, there is a dplyr function for this [{(slice_tail(dat, n=1)}](https://stackoverflow.com/questions/48208487/how-to-add-commit-message-using-vim)   






