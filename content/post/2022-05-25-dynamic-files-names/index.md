---
title: Dynamic Files Names
author: Kevin
date: '2022-05-25'
slug: []
categories:
  - R
tags:
  - Glue
image:
  caption: ''
  focal_point: ''
---


I am something now!

I realized my file names were going to overwrite each other unless I could find some way to make the them unique. 


```{r money}
openxlsx::saveWorkbook(global, here::here("budgets", glue(
  '{bp_set$client_id}'), glue("{date}_{bp_set$client_id}_updated-global.xlsx")), overwrite = TRUE)
```

I set the date variable like this:  date <- Sys.Date()

I think by day is unique enough but could add time as well.

time <- Sys.time()



```{r money}
openxlsx::saveWorkbook(global, here::here("budgets", glue(
  '{bp_set$client_id}'), glue("{date}_{time}_{bp_set$client_id}_updated-global.xlsx")), overwrite = TRUE)
```