---
title: Blogdown Tricks
author: kevin
date: '2020-04-02'
slug: blogdown-tricks
categories:
  - R
tags:
  - blogdown
image:
  caption: ''
  focal_point: ''
---

I came across this [page](https://maya.rbind.io/masteringshiny/example/) from Maya Gands over the weekend.

Besides being thankful for the content; I wanted to figure out how these pages were built. I loved the layout of the page with a sidebar! I wish to set up my Chromebook for Data Science projects like this.

Reviewing the [git repo](https://github.com/MayaGans/BayesianBabe/tree/master/content/MasteringShiny) it was not that complicated. 

In the blogdown file structure.

Under the content folder

* Make a new folder for the project
* Create an index.md file
* Add another folder with index.md?
* Add content. It appears in this case it is .md files; I wonder if .Rmd files would work. 


