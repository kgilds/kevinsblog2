---
title: Playing with Pagedown and Moon-Reader
author: Kevin
date: '2019-01-27'
slug: playing-with-pagedown-and-moon-reader
categories:
  - R
tags: []
header:
  caption: ''
  image: ''
---

This afternoon I downloaded the [pagedown package](https://pagedown.rbind.io/html-resume) and played with the resume template. I enjoyed working with package; there are some expected quirks such as headings needing to start on the same page to get the formatting just right. Resumes are typically a pain so it was par for the course and output is amazing.

A helpful-hint is that if you download the [xaringan package](https://slides.yihui.name/xaringan/#1) you can use the Infinite Moon Reader Addin to preview the output in the viewer. 



