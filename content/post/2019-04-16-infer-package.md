---
title: Infer Package
author: Kevin
date: '2019-04-16'
slug: infer-package
categories:
  - R
tags: ["Infer"]
image:
  caption: ''
  focal_point: ''
---

This morning, I was able to install the [Infer Package](https://github.com/tidymodels/infer)

This [video](https://www.rstudio.com/resources/videos/infer-a-package-for-tidy-statistical-inference/) is a great introduction. 