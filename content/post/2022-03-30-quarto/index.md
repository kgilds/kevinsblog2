---
title: Quarto
author: Kevin
date: '2022-03-30'
slug: []
categories:
  - R
tags: [quarto]
image:
  caption: ''
  focal_point: ''
---

It seem [Quarto](https://quarto.org/) is the next hot framework for producing analysis reports, books, websites, wikis in R, Python, Julia, and Observable. I finally was able to install on my computer. I can’t wait to try.

Here is a good demonstration:

https://www.youtube.com/watch?t=67&v=6p4vOKS6Xls&feature=youtu.be