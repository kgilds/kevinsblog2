---
title: Old Code
author: Kevin
date: '2022-05-06'
slug: []
categories:
  - R
tags:
  - funny
image:
  caption: ''
  focal_point: ''
---


I found this funny and familiar.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">When I&#39;m debugging something and someone sends me code that I myself have written 2 months ago referencing the problem <a href="https://t.co/OeavqTSvip">pic.twitter.com/OeavqTSvip</a></p>&mdash; Vicki (@vboykis) <a href="https://twitter.com/vboykis/status/1522217727573716993?ref_src=twsrc%5Etfw">May 5, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 