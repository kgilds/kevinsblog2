---
title: Is this the one
author: Kevin
date: '2019-02-14'
slug: is-this-the-one
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

> 4:30 am -6:00 am

Worked on making custom reports for an upcoming webinar. I have one file that is not working and will not work as a child document. I was looking at my tables again and I may want to pull the tiles out of the adorn title argument and just put the question in the caption.



Here is a link to a [Github](https://github.com/dtkaplan/shinymark/blob/master/03-up-your-rmd-game/Economist.Rmd) file that is an example of Parameterized reports. I think I understand what is happening in the code!. I can't wait to try. :smiley:

