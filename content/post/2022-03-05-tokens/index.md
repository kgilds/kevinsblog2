---
title: Tokens
author: Kevin
date: '2022-03-05'
slug: []
categories:
  - R
tags:
  - gitlabr
image:
  caption: ''
  focal_point: ''
---

My gitlab token had expired and I set out to create a new one. However, when I was working with the gitlab api from [gitlabR](https://statnmap.github.io/gitlabr/) I kept getting errors. I kept trying and trying different items, and then recalled I had set an project environmental variable. That turned out to be the error. 

My goal is to set this project set up to utilize this package. 