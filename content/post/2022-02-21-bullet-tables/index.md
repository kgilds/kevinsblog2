---
title: Bullet Tables
author: Kevin
date: '2022-02-21'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

Watched a thread on twitter regarding charts that convey target vs actual and saw this response.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">A more realistic use-case of the gtExtras::gt_plt_bullet() inside a {gt} table below. The core table is ~8 lines of code.<br><br>Several high-profile QBs struggled in Wk 1, 2021 (dropped &gt; 32 %-points from 2020).<br><br>Data: <a href="https://twitter.com/ESPNStatsInfo?ref_src=twsrc%5Etfw">@ESPNStatsInfo</a> &amp; <a href="https://twitter.com/hashtag/nflverse?src=hash&amp;ref_src=twsrc%5Etfw">#nflverse</a> <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> Code: <a href="https://t.co/1UQikNjFnx">https://t.co/1UQikNjFnx</a> <a href="https://t.co/OlKMjwwdKu">pic.twitter.com/OlKMjwwdKu</a></p>&mdash; Tom Mock (@thomas_mock) <a href="https://twitter.com/thomas_mock/status/1437792802495139852?ref_src=twsrc%5Etfw">September 14, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


I am going to try this on my next report!