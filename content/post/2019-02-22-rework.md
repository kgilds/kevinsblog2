---
title: Rework
author: Kevin
date: '2019-02-22'
slug: rework
categories:
  - management
tags:
  - management
header:
  caption: ''
  image: ''
---

Today, I binged on the [Rework](https://rework.fm/) podcast and learned about this video series from Episode 25 *Go Behind the Scenes.





<iframe width="560" height="315" src="https://www.youtube.com/embed/zh8Rjur18S8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


The episode also made me think of sharing my Git Repo; however, I suspect it may be over-sharing and it would not be clear the value of the sharing.


