---
title: Lookup with R
author: Kevin
date: '2019-09-16'
slug: lookup-with-r
categories:
  - R
tags:
  - merge
  - spreadsheets
image:
  caption: ''
  focal_point: ''
---

I was worried I would need to write a massive case when function. Thankfully, I found this [article](https://www.rforexcelusers.com/vlookup-in-r/). I can use the merge function and in my case specify all.x = TRUE. 

I will use [googledrive package](https://googledrive.tidyverse.org/) to update the merge with additional fields or easily update fields.  