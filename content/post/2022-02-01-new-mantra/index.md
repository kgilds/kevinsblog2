---
title: New Mantra
author: Kevin
date: '2022-02-01'
slug: []
categories:
  - R
tags:
  - Documentation
image:
  caption: ''
  focal_point: ''
---

This feels powerful and true

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Programming is how you code software. Documentation is how you code a business. Pretty sure I first heard this gem from the brilliant <a href="https://twitter.com/AngeBassa?ref_src=twsrc%5Etfw">@AngeBassa</a> 🙌🙌 <a href="https://t.co/4VJTY9tb6Y">https://t.co/4VJTY9tb6Y</a></p>&mdash; Tanya Cashorali (@tanyacash21) <a href="https://twitter.com/tanyacash21/status/1488517954497089544?ref_src=twsrc%5Etfw">February 1, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 