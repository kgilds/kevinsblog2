---
title: A Qualtrics Sunday
author: Kevin
date: '2019-03-31'
slug: a-qualtrics-sunday
categories:
  - R
tags: ["Qualtrics"]
image:
  caption: ''
  focal_point: ''
---

Today, I spent a lot of time working in Qualtrics. My time was spent trying to fire an Action based upon a duplicate value in a text field. What kept me motivated and frustrated was many false-positivies.

This did not work ~^[0-9]{3}[A-Za-z]{2}[0-9]{6}nn+~

Doing many google searchs I came across some packages to work with Qualtrics.

* [QualRics](https://github.com/ropensci/qualtRics)

* [Qualtric Tools](https://github.com/emmamorgan-tufts/QualtricsTools/)

* [qsurvey](https://github.com/emmamorgan-tufts/QualtricsTools/)