---
title: Encounters with Vim
author: Kevin 
date: '2020-11-06'
slug: encounters-with-vim
categories:
  - R
tags:
  - git
  - Vim
image:
  caption: ''
  focal_point: ''
---

I had a VIM encounter the other day when a merge into a branch required a commit message. A moment of panic but I got through it.

Here is a [stackoverflow post](https://stackoverflow.com/questions/48208487/how-to-add-commit-message-using-vim) that walks one through the process

1. type i to enter "Insert" Mode and write your commit message
2. Exit Insert Mode by pressing Escape
3. Now select :wq to save and exit. w for write and q for quit. An alias is :x 

Let me see if I remember this. 