---
title: Twitch
author: Kevin
date: '2021-06-06'
slug: []
categories:
  - R
tags: [Twitch]
image:
  caption: ''
  focal_point: ''
---

My 7 year old finally beat me in some races today in Mario Kart. 

> His explanation--I have been watching videos.


I like his style and find videos helpful in the learning process. The last few weeks I have been watching a lot of content on Twitch. I find it valuable to pick up on the styles of others.


Here is a [post](https://www.jessemaegan.com/blog/2021-05-28-data-science-twitch-streamers-round-up/) that highlights Data Science Streams.

Further there is the [Sliced Competition](https://www.twitch.tv/videos/1042917748?filter=archives&sort=time)

