---
title: File System
author: Kevin
date: '2022-03-31'
slug: []
categories:
  - R
tags: [files, fs]
image:
  caption: ''
  focal_point: ''
---


I liked this [post](https://github.com/Cghlewis/data-wrangling-functions/wiki/File-System) on using the file system. I appreciate using practical examples. 


Another handy function to use is the fs::dir_tree

```r
fs::dir_tree()
 
```


