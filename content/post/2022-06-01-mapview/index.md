---
title: Mapview
author: Kevin
date: '2022-06-01'
slug: []
categories:
  - R
tags: [mapview, tidycensus]
image:
  caption: ''
  focal_point: ''
---


I downloaded the [`mapview package`](https://r-spatial.github.io/mapview/) and it was an intense download. Can' wait to play with it. Good examples of use are found here https://walker-data.com/census-r/census-geographic-data-and-applications-in-r.html