---
title: Command Line Resource
author: R
date: '2022-01-24'
slug: []
categories:
  - Kevin
tags:
  - CLI
image:
  caption: ''
  focal_point: ''
---

I found this on twitter 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Lecture slides for my &#39;Data Science Programming Methods&#39; course STAT 447 from this Fall 2021 at U of Illinois are now accessible via <a href="https://t.co/hHewPJY3pl">https://t.co/hHewPJY3pl</a> covering shell (incl sed/awk), markdown, git(hub), sql, lots of <a href="https://twitter.com/hashtag/Rstats?src=hash&amp;ref_src=twsrc%5Etfw">#Rstats</a> up to packaging, and Docker. Enjoy! <a href="https://t.co/Dv6XSO1ZWj">pic.twitter.com/Dv6XSO1ZWj</a></p>&mdash; Dirk Eddelbuettel (@eddelbuettel) <a href="https://twitter.com/eddelbuettel/status/1484910046295306245?ref_src=twsrc%5Etfw">January 22, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

I found the [slides](https://stat447.com/lectures/01-shell/) on working within the shell helpful. 