---
title: Towards TRUE
author: Kevin
date: '2022-05-08'
slug: []
categories:
  - R
tags:
  - If-Statements
image:
  caption: ''
  focal_point: ''
---

> Make it more abstract


I had worked out a conditional statement that worked--but I needed something more abstract. My current iteration would work right here and now but changing data may break it. My previous `if` statement was based on the number of columns in the data frame but there was a small possibility results could be wrong. 


I worked to find a statement to test based on the names of column names in a data frame. I found this [post](https://statisticsglobe.com/check-if-column-exists-in-data-frame-in-r). However, I need to write a condition where the column name was not in the date frame to be `TRUE`. I recalled this [post](https://kgilds.rbind.io/post/2022-04-10-data-wrangling/). 


Below is a script that brings me the abstraction I was looking for. This script is dynamic to take on different use cases in the future.  The trick part was putting a `!` in front of the FALSE to make the statement `TRUE`, and hence work! 



```r
if(!FALSE %in% colnames(results_error_dat) == TRUE){
   
    gt::gt(results_error_dat) %>%
    cols_label(
      
      "TRUE" = "Success"
    ) %>%
    gt::tab_source_note("Success = increased outcome score/maintained a medium level") %>%
    gt::tab_source_note("Q-matches = the number of students with a pre and post score") %>%
    gt::tab_source_note("Stats = change between pre/post") %>%
    gswcf::gt_theme_538()
  
} else{
  
  
    gt::gt(results_error_dat) %>%
    gt::cols_label(
      "FALSE" = "In Progress",
      "TRUE" = "Success"
    ) %>%
    gt::tab_source_note("Success = increased outcome score/maintained a medium level") %>%
    gt::tab_source_note("Q-matches = the number of students with a pre and post score") %>%
    gt::tab_source_note("Stats = change between pre/post") %>%
    gswcf::gt_theme_538() 

}
```