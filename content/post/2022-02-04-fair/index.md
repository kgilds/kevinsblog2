---
title: FAIR
author: Kevin
date: '2022-02-04'
slug: []
categories:
  - R
tags:
  - WIC
image:
  caption: ''
  focal_point: ''
---

A useful guide for data management can be found [here](https://cghlewis.github.io/mpsi-data-training/training_00.html)

The FAIR Principles for scientific data management and stewardship are provided. I think these principles should apply to in evaluation. 

1. Findable: The move towards automation means metadata needs good documentation and machine readable.

2. Accessible: Data is available in a repository or though a request system

3. Interoperable: Humans and machines need to be able to read and understand the data. There should not be a barrier to the data in terms of a software license. Documentation should pair with the data to allow interoperable. 


4. Reusable: Good documentation on the context of the data and the data collection and clear license for data use. 