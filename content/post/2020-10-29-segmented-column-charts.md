---
title: Segmented Column Charts
author: Kevin
date: '2020-10-29'
slug: segmented-column-charts
categories:
  - R
tags:
  - plot2
image:
  caption: ''
  focal_point: ''
---

Some ggplot2 themes/packages I want to try.

The first one is [geom_chicklet](https://rud.is/b/2019/06/30/make-refreshing-segmented-column-charts-with-ggchicklet/) and [package website](https://cinc.rud.is/web/packages/ggchicklet/)

The [ggecondist package](https://cinc.rud.is/web/packages/ggeconodist/) provides a fancy way to display data distributions.

The [waffle package](https://github.com/hrbrmstr/waffle) is another option for reports. 
