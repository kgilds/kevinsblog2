---
title: Note-to-self
author: Kevin
date: '2022-02-23'
slug: []
categories:
  - R
tags: [testing, documentation, covrpage]
image:
  caption: ''
  focal_point: ''
---

A  `covrpage::covrpage` render failed in a package of mine this morning. I was messing with it tonight and I tried to re-install the package and wound up with an error. The error was that I did not have a title for a documented data set.

I fixed the documentation error and was able to re-install the package. I ran `covrpage` again and it works just fine again.  