---
title: Debugging Shiny
author: Kevin
date: '2019-11-07'
slug: debugging-shiny
categories:
  - R
tags:
  - Shiny
image:
  caption: ''
  focal_point: ''
---

I broke a Shiny Application this morning. The good news is that I revived the application by backtracking some code. I am still surprised what I wrote broke the application. I think what did was some empty call to plotly Output. 

```
plotlyOutput(ns(""))
```

However, I did stumble across this helpful article on [debugging Shiny Applications](https://shiny.rstudio.com/articles/debugging.html)
