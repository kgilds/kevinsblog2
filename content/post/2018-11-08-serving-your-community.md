---
title: Serving Your Community
author: Kevin
date: '2018-11-08'
slug: serving-your-community
categories: ["General"]
tags: []
header:
  caption: ''
  image: ''
---

I live in [Lakeland Florida](https://www.lakelandgov.net/), and sit on the civil service board. I highly recommend serving your community in this manner.  At times you have to make tough decisions about employee circumstances, however, you will learn a lot about how you make decisions and weigh evidence presented to you.  

