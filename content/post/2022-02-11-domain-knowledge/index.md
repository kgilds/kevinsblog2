---
title: Domain Knowledge
author: Kevin
date: '2022-02-11'
slug: []
categories:
  - Data science
tags: [WIC]
image:
  caption: ''
  focal_point: ''
---

And then there was SQL. Actually I need to bring this language up for discussion at some point.

This post will be about the importance of content and technical skills.

See this article: [The Data Scientist of the Future, According to Google](https://blog.devgenius.io/the-data-scientist-of-the-future-according-to-google-4d8545b37bdc)

   > Domain Knowledge will rule the world

The author holds that an understanding of the inputs and outputs of a model will be key and MBA students will be learning SQL.

I am biased toward the belief that domain knowledge is critical to help you understand the data. I want to believe this and It is my experience as well that context matters. I have strong diversion against completing visualizations or machine models if I don’t understand the context of the data.