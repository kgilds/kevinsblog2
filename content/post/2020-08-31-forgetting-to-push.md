---
title: Forgetting to push
author: Kevin
date: '2020-08-31'
slug: forgetting-to-push
categories:
  - R
  - Ubuntu
tags: [git]
image:
  caption: ''
  focal_point: ''
---

See this [post](https://kgilds.rbind.io/post/rstudio-trouble-shooting/) I had a different type of error with a self-developed package. I was not able to reinstall my own package with out completing an package update? in the terminal. I could not leave this workaround in place and crashed the operating system. 

Anyway, I cloned the project onto a Windows machine--ugh awful for a couple of reasons and determined to spin up an Rstudio on a digial ocean server. This worked well. However, I noticed I did not push a big change to my repo-duh. 

Typically, I am good at pushing updates to my repos. 