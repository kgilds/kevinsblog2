---
title: And more git-hooks
author: Kevin
date: '2022-01-26'
slug: []
categories:
  - Git
tags:
  - Git
image:
  caption: ''
  focal_point: ''
---

I edited my git-hook file to take out the if statement. Ultimately whenever I want to commit I want the Readme to render. Let us see what happens when we commit this file.  
