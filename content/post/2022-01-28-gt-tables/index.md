---
title: GT Tables
author: Kevin
date: '2022-01-28'
slug: []
categories:
  - R
tags: [tables]
image:
  caption: ''
  focal_point: ''
---


This is very good demonstration of using the [`gt` package](https://gt.rstudio.com/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/z0UGmMOxl-c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>