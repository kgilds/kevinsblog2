---
title: Vim Adventures
author: Kevin
date: '2019-05-25'
slug: vim-adventures
categories:
  - R
tags:
  - Git
  - Vim
image:
  caption: ''
  focal_point: ''
---

For those times when you forget to `-m "I forgot"` here is a [Stackoverflow Post](https://stackoverflow.com/questions/36782525/stuck-on-git-commit-window)

