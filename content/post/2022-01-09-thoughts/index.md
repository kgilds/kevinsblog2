---
title: Thoughts
author: Kevin
date: '2022-01-09'
slug: []
categories:
  - management
tags: []
image:
  caption: ''
  focal_point: ''
---

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Software people have NO idea how much of non-tech office work is copying and pasting data across software applications that aren&#39;t integrated but badly need to be. <a href="https://t.co/17inpH8l6y">https://t.co/17inpH8l6y</a></p>&mdash; Alex Kyllo (@alexkyllo) <a href="https://twitter.com/alexkyllo/status/1479875965023322112?ref_src=twsrc%5Etfw">January 8, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


My response was this

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I live in the non-tech space and figured everyone knew this. <a href="https://t.co/BlVNMQywwa">https://t.co/BlVNMQywwa</a></p>&mdash; Kevin Gilds (@Kevin_Gilds) <a href="https://twitter.com/Kevin_Gilds/status/1479916297660121091?ref_src=twsrc%5Etfw">January 8, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


Reflecting on this some more---

> My value as a worker has been finding solutions to these gaps. 



