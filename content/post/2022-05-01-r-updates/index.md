---
title: R-updates
author: Kevin
date: '2022-05-01'
slug: []
categories:
  - R
tags:
  - renv
  - docker
image:
  caption: ''
  focal_point: ''
---

This week I installed a mysql instance on my local machine and when I completed an update to my system–apparently I updated R. Typically in the past, I had to update R explicitly.

What is fun is that you get to reinstall all of your packages when you update R. It is truly a pain but can also make your prior projects non functioning! This happened to me last summer :sob:

Fortunately there is package for this [renv](https://rstudio.github.io/renv/index.html)

This is renv package is complicated but not hard to implement in your projects. The function hydrate reinstalled all the required libraries. The renv package is also provides you with an infrastructure for more advanced features such as Docker and GitHub Actions and Gitlab CI/CD.


The change to a different version of R  bit me again as this new version of R did not like a conditional statement that has been working fine for nearly two years. I spent my morning re factoring this code. Ultimately, I need this project running on a Docker instance to avoid these issues.