---
title: It happened
author: kevin
date: '2020-05-28'
slug: it-happened
categories:
  - R
tags: [Purr]
image:
  caption: ''
  focal_point: ''
---


It finally happened. I needed to read a directory of csv files.  I even applied the lesson to read a batch of excel files. 

I needed to use the [purr package](https://purrr.tidyverse.org/). This felt dangerous because the last time I used purr it was an experience of copying and pasting some code and hoping for the best.

This [post](https://www.gerkelab.com/blog/2018/09/import-directory-csv-purrr-readr/) helped me understand what I was doing and the capacity to add arguments. I was also able to apply the lesson to read a directory of excel files. 

