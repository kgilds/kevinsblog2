---
title: Code Ownership
author: 'Kevin '
date: '2022-02-20'
slug: []
categories:
  - Craftsmanship
tags: [WIC]
image:
  caption: ''
  focal_point: ''
---


Very tired today and thankful for this [article](https://vickiboykis.com/2022/02/21/on-owning-a-software-problem/) to chew on.  
What makes good craftsmanship in data science or analysis is the topic at hand. Vicki argues that in a good analysis a sense of ownership is apparent.

> A good developer cares about the code they’re putting out into the world not only because it has to work for them, but because their code will spend most of its time being read and implemented and worked on by others, and as developers, we want to care about those people, too

My spin is that I want the product/analysis to work for the subject matter expert who can recognize nuance.

This conversation is making me think of the book _Football Dreams_ about a kid who just hopes another former offensive lineman recognizes his work.