---
title: File Paths
author: Kevin
date: '2022-03-01'
slug: []
categories:
  - R
tags:
  - fs
  - file-path
image:
  caption: ''
  focal_point: ''
---

My most popular tweet was  regarding file paths
<blockquote
 class="twitter-tweet"><p lang="en" dir="ltr">having the talk 
with my 11 year old about file paths.</p>&mdash; Kevin Gilds 
(@Kevin_Gilds) <a 
href="https://twitter.com/Kevin_Gilds/status/1406269668067794946?ref_src=twsrc%5Etfw">June
 19, 2021</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" 
charset="utf-8"></script>`


Understanding file paths were one of the most challenging aspects of learning to code. Thankfully, I had a frame of reference to understand a file directory. See this article from the Verge https://www.theverge.com/22684730/students-file-folder-directory-structure-education-gen-z. where some may not have even seen a typical directory structure. 




I will be perfectly honest, I am not sure I would have absorbed the knowledge. I needed context to learn and experience file paths. It is an important topic and often overlooked with intro to coding. 
