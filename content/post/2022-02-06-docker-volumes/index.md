---
title: Docker Volumes
author: Kevin
date: '2022-02-06'
slug: []
categories:
  - Docker
tags:
  - CLI
image:
  caption: ''
  focal_point: ''
---


Yesterday, while going through the lessons from [Data Science at the Command Line](https://datascienceatthecommandline.com/2e/chapter-2-getting-started.html), I learned how to connect a local directory with a docker container. It is pretty straightforward with this  basic structure. 

```
docker run --rm -it -v "$(pwd)":/data datasciencetoolbox/dsatcl2e
```

My pending question is if you have to run this particular script every time you run the container. If I run the container without this script is there still a connection.  Later in the book there is a section on copying files into the container. Which is the best method?  