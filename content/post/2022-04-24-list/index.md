---
title: List
author: Kevin
date: '2022-04-24'
slug: []
categories: []
tags: []
image:
  caption: ''
  focal_point: ''
---

This list feels familiar:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">20 things I&#39;ve learned over the years as a systems (over)thinker ... <br><br>(some advice I gave a friend, that may be helpful) <a href="https://t.co/RAfkeap5oK">pic.twitter.com/RAfkeap5oK</a></p>&mdash; John Cutler (@johncutlefish) <a href="https://twitter.com/johncutlefish/status/1518361842342453248?ref_src=twsrc%5Etfw">April 24, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 