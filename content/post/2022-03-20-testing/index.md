---
title: Testing
author: Kevin
date: '2022-03-20'
slug: []
categories:
  - R
tags:
  - covrpage
  - gitlabr
image:
  caption: ''
  focal_point: ''
---

As follow up to this [post](https://kgilds.rbind.io/post/2022-03-18-testing-pipelines/) I was able to install `covrpage` after I provided gitlab with my Github PAT and the report ran. However, I have not figured out how to render the report output for gitlab to place in the artifact folder. Maybe I can find a way to automate the process.  


