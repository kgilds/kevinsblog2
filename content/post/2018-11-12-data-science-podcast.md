---
title: Discovery
author: Kevin
date: '2018-11-12'
slug: data-science-podcast
categories:
  - R
tags:
  - podcast
header:
  caption: ''
  image: ''
---

A couple neat discoveries this weekend.

The [Credibly Curious Podcast](https://soundcloud.com/crediblycurious)

and the [learnr package](https://rstudio.github.io/learnr/). The intent of this package is for tutorials but I see a potential of related Rmarkdown documents.  


