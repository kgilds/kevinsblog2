---
title: Excel
author: ''
date: '2022-04-15'
slug: []
categories:
  - R
tags:
  - spreadsheets
image:
  caption: ''
  focal_point: ''
---

This tweet

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Knowing when you’ve outgrown excel needs to be explicitly taught imo.</p>&mdash; JD Long (@CMastication) <a href="https://twitter.com/CMastication/status/1514580379805622284?ref_src=twsrc%5Etfw">April 14, 2022</a></blockquote>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

feels right🍻
