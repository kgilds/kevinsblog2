---
title: Table Captions in Bookdown
author: Kevin
date: '2020-01-20'
slug: table-captions-in-bookdown
categories:
  - R
tags:
  - bookdown
image:
  caption: ''
  focal_point: ''
---

I am currently working on a report in [bookdown](https://bookdown.org/). This year I committed to working to working through my challenges with bookdown. I made a determination I am not ready for the PDF output but I have made progress in getting a report in MS Word and in HTML. 

My first vexing issue was a Table Caption was not rendering correctly. I figured the issue was a subtle mistake I was making; I was correct--my chunk label was not correct. Chunk labels need to be in this form ```lang-arts-history```
Not ```lang_arts_history`` for the table caption to work. I scoured Stackoverflow and R-Studio Community.

My other challenge with working with Bookdown is working with MS Word. Every edit to content resulted in a new download and updated formatting to the Word document; I used this [article](https://rmarkdown.rstudio.com/articles_docx.html) to help me create a reference document to implement styles for every download. Thus far it seems like it will be less painful to edit content and download to Word as I was finally able to get line spacing correct and to indent the first paragraph. 
