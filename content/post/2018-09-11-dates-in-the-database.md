---
title: Dates in the database
author: Kevin
date: '2018-09-11'
slug: dates-in-the-database
categories:
  - Access
  - Database
tags: ["Access"]
header:
  caption: ''
  image: ''
---

I am learning to parse dates in database queries at work. I had discovered the DatePart function in Access but I soon realized that the function is limited to only extracting one part of the date at a time. I was running queries based on the month but soon realized I would need the month and year. 

I discovered the Format function would provide more flexibility in returning multiple date options such as month and year.

The structure of the argument is 


```
Format([Field], "mm/yyyy")
```


```
DatePart("m",[Field])

```
