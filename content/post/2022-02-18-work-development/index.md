---
title: Work Development
author: Kevin
date: '2022-02-18'
slug: []
categories:
  - management
tags:
  - WIC
image:
  caption: ''
  focal_point: ''
---


The smart people on twitter were talking about this article and wanted to check it out. 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I have a new research report out. It is free &amp; you can have it!! It&#39;s about writing code and &quot;Learning Debt&quot;: a way to describe how *even tech teams focused on productivity* often fail to support learning. But I&#39;ll also tell you about it here! 🧵<a href="https://t.co/Yp55crGRvK">https://t.co/Yp55crGRvK</a> <a href="https://t.co/vCmsOOzqKU">pic.twitter.com/vCmsOOzqKU</a></p>&mdash; Dr. Cat Hicks 📈🦄🏳️‍🌈 (@grimalkina) <a href="https://twitter.com/grimalkina/status/1493321929331068929?ref_src=twsrc%5Etfw">February 14, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

I code alone but some of the concepts hit in other context especially learning models stand out in this [report]( https://www.catharsisinsight.com/_files/ugd/fce7f8_2a41aa82670f4f08a3e403d196bcc341.pdf)


My favorite recommendation include: 

1. Reward documentation!
2. Involve people in how their success is measured
3. Encourage developmental feedback
