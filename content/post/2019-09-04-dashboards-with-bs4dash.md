---
title: Dashboards with bs4Dash
author: Kevin
date: '2019-09-04'
slug: dashboards-with-bs4dash
categories:
  - R
tags: [bs4Dash]
image:
  caption: ''
  focal_point: ''
---

I have been using the [bs4Dash Package](https://github.com/RinteRface/bs4Dash/issues); below is a webinar with the author of the package from the [Shiny Developer Series](https://blog.rstudio.com/2019/08/05/the-shiny-developer-series/).


<iframe width="560" height="315" src="https://www.youtube.com/embed/0oi6tGzHUHM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>