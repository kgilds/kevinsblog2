---
title: Git merge --abort
author: Kevin
date: '2019-04-13'
slug: git-merge-abort
categories:
  - R
tags: ["Git"]
image:
  caption: ''
  focal_point: ''
---

I had a git melt down this morning. This [Happy Git UseR](https://happygitwithr.com/). This worked 
``r
git merge --abort  
``

In addition, I learned how to cache my credinitals on linux system.

``r
git config --global credential.helper 'cache --timeout=10000000'
``

I also need to be tagging my commits and updates but can't seem to get into the workflow.

``r
git tag -a v1.4 -m "my version 1.4"
``

You can also tag commits after the fact. The Happy Git UseR has a good discussion but not much details.

This [link](https://git-scm.com/book/en/v2/Git-Basics-Tagging) has detailed examples of taggings.



``r
git log --pretty=oneline
``

``r
git tag -a v1.2 9fceb02
``
