---
title: Design Matters
author: Kevin
date: '2022-03-19'
slug: []
categories:
  - R
tags:
  - automation
image:
  caption: ''
  focal_point: ''
---

Design matters in an analysis project. One of the most important aspects is in coordinating the data entry process. I shoot for win/win for the person having to enter the information and for the person having to make sense of the data. 


<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I&#39;m seeing people begin to talk about data collection as the elephant in the room, which is good, but I wish we&#39;d go further than &quot;data people should try collection sometime ;)&quot; to something stronger. idk what that should be yet <a href="https://t.co/dtgZlTycwK">pic.twitter.com/dtgZlTycwK</a></p>&mdash; Hamilton Ulmer (@hamiltonulmer) <a href="https://twitter.com/hamiltonulmer/status/1505220182561624065?ref_src=twsrc%5Etfw">March 19, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 