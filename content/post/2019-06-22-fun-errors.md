---
title: Fun-Errors
author: Kevin
date: '2019-06-22'
slug: fun-errors
categories:
  - R
tags:
  - R-Package
image:
  caption: ''
  focal_point: ''
---

This morning, I updated a package and received a strange error.

**something about tidyverse not being loaded**

What? 

I put a data processing script in the R directory. Sad but at least I figured it out. 