---
title: Hosting Shiny Applications
author: Kevin
date: '2022-03-28'
slug: []
categories:
  - R
tags:
  - docker
  - digital ocean 
image:
  caption: ''
  focal_point: ''
---


This is an interesting [post](https://hosting.analythium.io/how-to-host-shiny-apps-on-the-digitalocean-app-platform/). This example using docker hub as the source. I wonder if Github and or Gitlab would be just as easy!

