---
title: Skill Base
author: Kevin
date: '2022-03-24'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


This is a good [post](https://vickiboykis.com/2019/02/13/data-science-is-different-now/)

1. Learn SQL

2. Pick a language R, Python etc

3. Learn to work in the cloud


I need more practice with SQL in the wild and more experience with working in the cloud. 