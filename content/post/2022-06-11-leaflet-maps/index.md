---
title: Leaflet Maps
author: Kevin
date: '2022-06-11'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


This is the best intro to the [leaflet r package](https://rstudio.github.io/leaflet/) I have come across. 
https://mapping-in-r.netlify.app/ 

I had a hard time getting started with this mapping package. Most of the other tutorials seemed very complex,  when I just wanted to get a basic map up and running.   

What I came away with is this: 

For a basic map you can use a dataset that has lat and long variables

However if you require a choropleths map you need a more advanced dataset with shapefiles.
