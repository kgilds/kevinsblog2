---
title: Fun with Docker
author: Kevin
date: '2020-07-06'
slug: fun-with-docker
categories:
  - R
tags: [docker]
image:
  caption: ''
  focal_point: ''
---

I want to play with Docker but need to get some fundamentals down straight. A couple of reasons, I want to utilize [Shiny Studio](https://hub.docker.com/r/dm3ll3n/shinystudio) and I would also like to use Continuous Development functions from Gitlab. 

Here are some fundamental Docker commands that I took from this [tutoral](https://ropenscilabs.github.io/r-docker-tutorial/05-dockerfiles.html) by [rOpenSci Labs](https://github.com/ropenscilabs)

The first *FROM*

The docker image you want to use. 

```
FROM rocker/verse:3.3.2

```

The *RUN* command executes shell commands. Common usage would include install required packages. 
```
RUN R -e "install.packages('gapminder', repos = 'http://cran.us.r-project.org')"

```
The *ADD* command allows you to add files 
```
ADD analysis.R /home/rstudio/
```

Build the image 

```
docker build -t my-r-image .

```

Launch the image. 

```
docker run --rm -p 8787:8787 my-r-image

```

The period 


Below is a .gitlab-cy.yml file; it seems there are some similarities between the Docker commands above.

```
image: rocker/verse:3.4.1

pages:
  stage: deploy
  script:
  - Rscript -e "bookdown::render_book('index.Rmd', 'all', output_dir = 'public')"
  artifacts:
    paths:
    - public
  only:
  - master
```

I tried these steps on my Linux box and was getting a permission denied error. An extra step of creating a user group is required.

The [video](https://www.digitalocean.com/community/questions/how-to-fix-docker-got-permission-denied-while-trying-to-connect-to-the-docker-daemon-socket) in this tutorial is what helped me figure it out. 


I will save for another post on why in the world you would want to do these things!