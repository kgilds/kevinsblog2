---
title: Waiter for Shiny
author: Kevin
date: '2020-07-08'
slug: waiter-for-shiny
categories:
  - R
tags: [shiny]
image:
  caption: ''
  focal_point: ''
---

The [Shiny Developer Series](https://shinydevseries.com/)is back featuring [John Cocene](https://github.com/JohnCoene)!

<iframe width="560" height="315" src="https://www.youtube.com/embed/DOjbKYJ8R_s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here are some interesting packages he has developed.

[Waiter](https://waiter.john-coene.com/#)

[echarts4R](https://echarts4r.john-coene.com/index.html)

Key points:
 
 > make the application about the user not other developers.
