---
title: Package Resource & Example
author: Kevin
date: '2019-05-24'
slug: package-resource-example
categories:
  - R
tags:
  - R-Package
image:
  caption: ''
  focal_point: ''
---

My pressing concern about building an analysis packages is the large number of scripts to process all the data. Apparently this should go in a data-raw folder. Initially I set up a sub-directory of the R folder--this is a no-no.

The [babyname](https://github.com/hadley/babynames)