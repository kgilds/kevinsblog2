---
title: Sync Rmd files with Google Docs!
author: Kevin
date: '2019-03-04'
slug: sync-with-google-docs
categories:
  - R
tags: [rmdrive]
header:
  caption: ''
  image: ''
---

I am still struggling to find a way to sync between Rmd files and editing my narrative. I stumbled across this on twitter today. :grin:  The [rmdrive](https://github.com/ekothe/rmdrive) 


<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Quick demo of the rmdrive 📦workflow - create rmarkdown document in R, add text on google docs, resync to local rmd, knit. <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> <a href="https://t.co/h09TI8q4w0">pic.twitter.com/h09TI8q4w0</a></p>&mdash; Dr Emily Kothe (@emilyandthelime) <a href="https://twitter.com/emilyandthelime/status/1102015059486621697?ref_src=twsrc%5Etfw">March 3, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


I can't wait to try. 
