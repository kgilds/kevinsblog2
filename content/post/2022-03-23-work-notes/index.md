---
title: Work Notes
author: Kevin
date: '2022-03-23'
slug: []
categories:
  - R
tags: [tidyr, stringr, janitor]
image:
  caption: ''
  focal_point: ''
---

This morning I was working with some ugly data and came across this error message when I tried to run a [`janitor::tably()`](http://sfirke.github.io/janitor/) function. 

> Error: Each row of output must be identified by a unique combination of keys.
Keys are shared for 10 rows:
* 16, 21
* 17, 22
* 18, 23
* 19, 24
* 20, 25


My hunch was that it was the `NA` values in the dataset causing the trouble. 

I tried the [`tidyr::replace_na`](https://tidyr.tidyverse.org/reference/replace_na.html) but that did not work. I suspect `R` was treating the NA value as a character value.  


What did work was the [`stringr::str_replace_na`](https://stringr.tidyverse.org/reference/str_replace.html) function. 
