---
title: Table Gallery
author: Kevin
date: '2022-04-25'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

Check out these beautiful tables at the [RStudio Table Gallery](https://www.rstudio.com/blog/rstudio-community-table-gallery/)