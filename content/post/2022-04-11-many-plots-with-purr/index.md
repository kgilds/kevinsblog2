---
title: Many Plots with Purr
author: Kevin
date: '2022-04-11'
slug: []
categories:
  - R
tags:
  - purrr
image:
  caption: ''
  focal_point: ''
---


This is a great post on [`purr` ](https://aosmith.rbind.io/2018/08/20/automating-exploratory-plots/)and using plots. I was able to use this. The article is structured well to provide the reader with building blocks to start simple and builds to more complex and useful scripts.
