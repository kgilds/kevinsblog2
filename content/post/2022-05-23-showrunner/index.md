---
title: Showrunner
author: Kevin
date: '2022-05-23'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


Simon Willison https://twitter.com/simonw

Posted this article on general management–having a great vision and communicating to your team in the context of keeping a show running.

http://okbjgm.weebly.com/uploads/3/1/5/0/31506003/11_laws_of_showrunning_nice_version.pdf