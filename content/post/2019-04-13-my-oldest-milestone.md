---
title: My oldest Milestone
author: Kevin
date: '2019-04-13'
slug: my-oldest-milestone
categories:
  - R
tags: ["Chromebook Data Science", "RStudio Conference 2019"]
image:
  caption: ''
  focal_point: ''
---

According to my workflowy document Parameterized reports have been on my developmental list since 02/05/2017. I tried every which way with no success.


I finally successfully implemented last Sunday April 7, 2019.  Two resources helped [Chromebook Datascience](https://jhudatascience.org/chromebookdatascience/cbds.html) and this talk at the [RStudio Conference 2019](https://resources.rstudio.com/rstudio-conf-2019/the-lazy-and-easily-distracted-report-writer-using-rmarkdown-and-parameterised-reports)


When I watched the video presentation; I made the connection with a shiny input. This may not be a great connection but it worked. 
