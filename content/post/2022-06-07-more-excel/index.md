---
title: More Excel
author: Kevin
date: '2022-06-07'
slug: []
categories:
  - R
tags: [excel]
image:
  caption: ''
  focal_point: ''
---


I  often have to walk folks through their first time Excel doing this to them and having to use Notepad to change a value. In my case a trace number is converted to scientific notation.  

<blockquote
 class="twitter-tweet"><p lang="en" dir="ltr">my favorite 
feature of excel is &quot;this looked like a big number so we 
rounded it for you&quot; when its a primary key  and someone opened 
and saved the csv.</p>&mdash; Jacob Matson (@matsonj) <a 
href="https://twitter.com/matsonj/status/1534229523415171074?ref_src=twsrc%5Etfw">June
 7, 2022</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" 
charset="utf-8"></script>