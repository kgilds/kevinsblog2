---
title: More command line resources
author: Kevin
date: '2022-01-29'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


Here is another [command line tool resource](https://datascienceatthecommandline.com/2e/). I pulled down the docker image tonight and hope to work with it soon. Typically these command line resources start pretty basic,  and then they want you to draw the rest of owl pretty quickly. 