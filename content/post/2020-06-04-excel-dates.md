---
title: Excel Dates
author: kevin
date: '2020-06-04'
slug: excel-dates
categories:
  - R
tags:
  - janitor
image:
  caption: ''
  focal_point: ''
---

The [Janitor Package](http://sfirke.github.io/janitor/) is gold for anyone who has to work with Microsoft Excel files. 

I stumbled across a reference to this function this morning.

```
excel_numeric_to_date(
  date_num,
  date_system = "modern",
  include_time = FALSE,
  round_seconds = TRUE,
  tz = ""
)
```
