---
title: Select Columns with base R
author: Kevin
date: '2019-12-29'
slug: select-columns-with-base-r
categories:
  - R
tags:
  - base-r
image:
  caption: ''
  focal_point: ''
---

Sometimes knowing fundamentals in base r is practical especially working in Shiny. 

This subsets columns 1:4 of the gr_activity object. 


```(gr_activity[,1:4]```

If you had to mix match columns you need to add the concatenate function.


This spinet selects column 1, five and 6.

```(gr_activity[,c(1,5:6)])```

Cheers. 