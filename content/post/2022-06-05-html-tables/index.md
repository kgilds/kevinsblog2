---
title: html tables
author: Kevin
date: '2022-06-05'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---




How to organize non-rectangular data in a rmarkdown document. You create the tables and place the data in the cell with inline code. I tried using markdown tables but the complexity of the tables made me have to write the table structure in old-school html. 