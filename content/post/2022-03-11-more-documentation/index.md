---
title: 'More documentation '
author: Kevin
date: '2022-03-11'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


I remind myself that you never regret keeping a notebook or writing a note.

More this

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Reminder to everyone working with data -- you won&#39;t remember the thing you thought you&#39;d remember. Write it down. Make documentation. <br><br>It&#39;s not just nice to coworkers and colleagues. Future you will remember you much more kindly ;)</p>&mdash; Tom Carpenter (@tcarpenter216) <a href="https://twitter.com/tcarpenter216/status/1501099437367906304?ref_src=twsrc%5Etfw">March 8, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 