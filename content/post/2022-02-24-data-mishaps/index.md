---
title: Data Mishaps
author: Kevin
date: '2022-02-24'
slug: []
categories:
  - R
tags: [conference]
image:
  caption: ''
  focal_point: ''
---


Attended this [event](https://datamishapsnight.com/agenda-2022/) tonight.


My highlights include:

* Wrong spelling of a variable name

* Bad file names.

* A cp command that wipes out an entire directory.
