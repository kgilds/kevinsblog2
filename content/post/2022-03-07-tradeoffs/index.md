---
title: Tradeoffs
author: Kevin
date: '2022-03-07'
slug: []
categories:
  - R
tags:
  - WIC
image:
  caption: ''
  focal_point: ''
---


Trade offs I am not sure we are always explicit when we determine which tools we are using or wish to use. I am pointing to the person who would rather stick with a spreadsheet because it is comfortable and maybe initially faster.

Learning to code is investment in time and it helps you be more computer literate or as I say think like a computer. If you can script it; then you can make a function out of it, you can test assumptions, and automate the process.