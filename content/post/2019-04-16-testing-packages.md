---
title: Trying New Packages
author: Kevin
date: '2019-04-16'
slug: testing-packages
categories:
  - R
tags:
  - WorkNotes
image:
  caption: ''
  focal_point: ''
---

I downloaded and played with the functions in the [qualtRics package](https://github.com/ropensci/qualtRics) this morning.  

I am not sure I am going to incorporate into my current project workflow but any new projects involving qualtrics will incorporate this package. 

To get the most out of this package; it seems critical to get the API access; I was hoping for some additional functions to get a similar function to survey_questions for the csv file download option. 