---
title: CLT Notes
author: Kevin
date: '2022-01-30'
slug: []
categories:
  - Data science
tags:
  - CLI
image:
  caption: ''
  focal_point: ''
---


Notes from [Data Science from the Command Line](https://datascienceatthecommandline.com/2e/chapter-2-getting-started.html)

Four parts to the environment of command line use.

Command line tools are `ls`, `cat`, and `jq` . 

The 'terminal' is the place we type commands. 

The shell is what interprets the commands.

The operating system or the kernel executes the command line tools. 