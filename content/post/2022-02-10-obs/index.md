---
title: OBS
author: Kevin
date: '2022-02-10'
slug: []
categories:
  - R
tags: [OBS]
image:
  caption: ''
  focal_point: ''
---

Here is a stream on setting up [OBS](https://obsproject.com/)

I found this helpful, and I scrambled to find it when I wanted to share earlier. 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fkvQ_lAGvFQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>