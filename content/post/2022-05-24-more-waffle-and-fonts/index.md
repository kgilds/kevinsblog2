---
title: More Waffle and Fonts
author: Kevin
date: '2022-05-24'
slug: []
categories:
  - R
tags:
  - waffle
  - fonts
image:
  caption: ''
  focal_point: ''
---


Every so often, I have to re install the fonts to make my waffle charts work. 

See this [post](https://kgilds.rbind.io/post/2022-03-02-waffles-and-fonts/)


This time I had some more trouble and found this [issue](https://github.com/wch/extrafont/issues/32) discussion on github. 


This worked


```r
library(remotes)
remotes::install_version("Rttf2pt1", version = "1.3.8")

library(extrafont)
font_import()

```

