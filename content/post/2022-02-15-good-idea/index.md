---
title: Good Idea
author: Kevin
date: '2022-02-15'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

I am never sure what to tag on my site but this is a great idea. 


<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Picking good consistent tags for your technical website is hard -- it&#39;s easier to just parse out the packages in each post and use those:<a href="https://t.co/GEeOFyK4jS">https://t.co/GEeOFyK4jS</a>… <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> <a href="https://twitter.com/hashtag/blogdown?src=hash&amp;ref_src=twsrc%5Etfw">#blogdown</a> <a href="https://t.co/vUk48gL1vM">pic.twitter.com/vUk48gL1vM</a></p>&mdash; Bryan Shalloway (@brshallo) <a href="https://twitter.com/brshallo/status/1493143780886597634?ref_src=twsrc%5Etfw">February 14, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 