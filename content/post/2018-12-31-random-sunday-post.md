---
title: Random Sunday Post
author: Kevin
date: '2018-12-31'
slug: random-sunday-post
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

It has been a rough time getting work done between myself being sick and the kids at home being sick.  This morning was very unfocused but I managed to get some easy stuff done. I managed to update tables in the markdown report to make them look nice. Just recall to add the "html" argument. I did some deep dive into density plot making it really pretty however, I suspect it may too complex for my audience. I may keep it as it will be helpful when writing the evaluation report.


* need to update the 17-18 data for comparison purposes.

* when to update the Shiny Dashboard. 

