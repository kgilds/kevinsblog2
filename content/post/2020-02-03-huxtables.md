---
title: Huxtables
author: kevin
date: '2020-02-03'
slug: huxtables
categories:
  - R
tags:
  - bookdown
  - huxtable
image:
  caption: ''
  focal_point: ''
---


My adventure using bookdown is going well as I have been able to review my report online with [Netlify](https://www.netlify.com/). 


Ultimately, I need to output  this report to word and I required another option for displaying tables. I settled on the [huxtable package](https://hughjonesd.github.io/huxtable/). This will display tables nicely in html and does ok with a conversion to MS Word. Switching to the huxtable required that I use an alternative method for referencing and using table captions.


Here was the form that was required:

<caption> (\#tab:ae-parent-tbl-hux)Survey Question </caption>

```{r ae-parent-tbl-hux}
huxtable::as_huxtable(parent_tbl, add_colnames = TRUE) %>%
	huxtable::set_bold(1, 1:3, TRUE)

```



