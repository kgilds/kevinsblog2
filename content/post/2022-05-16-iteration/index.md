---
title: Iteration
author: Kevin
date: '2022-05-16'
slug: []
categories:
  - R
tags:
  - SQL
  - purrr
image:
  caption: ''
  focal_point: ''
---


I had liked a link to this [article](https://www.johnmackintosh.net/blog/2022-04-28-purrr-sql/) on twitter and then forgot about; however, I have been looking for this tutorial. 

This [config package](https://rstudio.github.io/config/) is important 