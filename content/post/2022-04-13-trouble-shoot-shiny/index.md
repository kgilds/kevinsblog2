---
title: Trouble Shoot Shiny
author: Kevin
date: '2022-04-13'
slug: []
categories:
  - R
tags:
  - Shiny
image:
  caption: ''
  focal_point: ''
---


I ran into trouble with a Shiny Application that was not updating data from local to shinyappsio. To be fair on my local machine I had trouble getting a consistent number. I renamed the data object with suffix of 1. I made the application correct but I will not know if I fixed the pipeline until new data comes in.


Anyhow here is a link to a tool, I was using this weekend. https://shiny.rstudio.com/articles/debugging.html




