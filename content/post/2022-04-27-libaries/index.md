---
title: Libraries
author: Kevin
date: '2022-04-27'
slug: []
categories:
  - R
tags:
  - R
  - WIC
image:
  caption: ''
  focal_point: ''
---


This is funny. I was writing text to explain packages and libraries to those new to programming.  

<blockquote class="twitter-tweet"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/RStats?src=hash&amp;ref_src=twsrc%5Etfw">#RStats</a>: Name things consistently and clearly<br><br>Also <a href="https://twitter.com/hashtag/RStats?src=hash&amp;ref_src=twsrc%5Etfw">#RStats</a>: A library of functions is called a package. But never ever ever refer to it as a library. You load a package with the command &quot;library()&quot;<br><br>Note: I call them libraries or packages. I don&#39;t care about your damn rules! <a href="https://t.co/KGgJFf2tbG">pic.twitter.com/KGgJFf2tbG</a></p>&mdash; mikefc (@coolbutuseless) <a href="https://twitter.com/coolbutuseless/status/1519434089455144960?ref_src=twsrc%5Etfw">April 27, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 