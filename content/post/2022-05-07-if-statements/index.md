---
title: If Statements
author: Kevin
date: '2022-05-07'
slug: []
categories:
  - R
tags:
  - functions
  - if
image:
  caption: ''
  focal_point: ''
---


I have been working on a script that creates a table based on a condition. Currently the limitation of my current `if` statement is that is hard wired based on a particular grant. This works as long as the data does not change, and I currently have dynamic data that may change from day to day. 


I have improved my conditional statement, but I would love to abstract it some more. Currently one table is produced if there are two columns and another table is produced if there are three columns. Ideally I would condition the table based on the name of the column in the date frame.  