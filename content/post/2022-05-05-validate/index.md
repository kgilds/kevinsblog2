---
title: Validate
author: Kevin
date: '2022-05-05'
slug: []
categories:
  - R
tags: [Shiny, validate]
image:
  caption: ''
  focal_point: ''
---


I studied this article today https://shiny.rstudio.com/articles/validation.html. Good news I suspect I have a temporary issues but so is my solution. 

I have a table that is expected to return two columns (TRUE, FALSE) but if all the students achieve success the table errors out. You may be able to put in a validation at this step in the pipeline?   I am depending on the count of the data to mask my issue for the time being.  