---
title: Updating R
author: Kevin
date: '2022-05-02'
slug: []
categories:
  - R
tags:
  - data managment
image:
  caption: ''
  focal_point: ''
---


Updating `R` can be a pain here is way to avoid all the library conflicts

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Yes! Set (in <a href="https://t.co/BiLm1HuVC9">https://t.co/BiLm1HuVC9</a>, or your personaly ~/.Rprofile) a library location _outside_ the default path of <a href="https://twitter.com/hashtag/Rstats?src=hash&amp;ref_src=twsrc%5Etfw">#Rstats</a>. Install there. <br><br>When you remove and upgrade R this library path will then unaffected -- which saves time at upgrades such as 4.1.* to 4.2.0.</p>&mdash; Dirk Eddelbuettel (@eddelbuettel) <a href="https://twitter.com/eddelbuettel/status/1521250491321270273?ref_src=twsrc%5Etfw">May 2, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 