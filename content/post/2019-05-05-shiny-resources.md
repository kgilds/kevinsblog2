---
title: Shiny Resources
author: Kevin
date: '2019-05-05'
slug: shiny-resources
categories:
  - R
tags: [Shiny]
image:
  caption: ''
  focal_point: ''
---

I stumbled across the [Shiny Developers Series Today](https://shinydevseries.com/). One resource, I want to try right away is `[shiny helper](https://github.com/cwthom/shinyhelper)  Check out the [demo](https://cwthom94.shinyapps.io/shinyhelper-demo/)
