---
title: Packages To Checkout
author: Kevin
date: '2019-07-21'
slug: packages-to-checkout
categories:
  - R
tags:
  - R-Packages
image:
  caption: ''
  focal_point: ''
---


Some interesting R-packages I found on twitter recently.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Your underrated <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> package today is &quot;ids&quot; by <a href="https://twitter.com/rgfitzjohn?ref_src=twsrc%5Etfw">@rgfitzjohn</a> which allows you to create unique identifiers quickly and easily <a href="https://t.co/nVTmFOM2DF">https://t.co/nVTmFOM2DF</a><br><br>like here, i create a list of &quot;adjective_animal&quot; names to support an anonymous selection process <a href="https://t.co/wabRIJiZWh">pic.twitter.com/wabRIJiZWh</a></p>&mdash; Andr(é|ew) MacDonald (@polesasunder) <a href="https://twitter.com/polesasunder/status/1151436678453714944?ref_src=twsrc%5Etfw">July 17, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

The [package site](https://richfitz.github.io/ids/) is here. 


Here is an R-Studio Add in that seems useful

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">This is an <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> RStudio Addin that I&#39;ve been using for creating default project folders and moving files into those folders based on extension rules. Perhaps someone else will find it useful too. <a href="https://t.co/62jBkbtSMg">https://t.co/62jBkbtSMg</a> <a href="https://t.co/eVXaSM4QRo">pic.twitter.com/eVXaSM4QRo</a></p>&mdash; joranelias (@joranelias) <a href="https://twitter.com/joranelias/status/1152316288615628800?ref_src=twsrc%5Etfw">July 19, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


Here is the [github page](https://github.com/joranE/projectDirs)
