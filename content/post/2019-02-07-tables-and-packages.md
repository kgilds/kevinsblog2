---
title: Tables and Packages
author: Kevin
date: '2019-02-07'
slug: tables-and-packages
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

> 4:12 am - 6:00 am

Cleaned up tables in Status report, they needed `type = html`. Since, the report will be rendered in html I made the subsections into tabs.

I started working on getReal2 package. I added the function hr_pre_range; however there was some strange things going on in the package. I may have installed the package from the branch and the master branch did not update. I made some tweaks and every thing seems fine by the time I wrapped things up. I scouted some scripts from the Import file that may serve as function.

