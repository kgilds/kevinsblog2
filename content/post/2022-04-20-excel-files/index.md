---
title: Excel Files
author: Kevin
date: '2022-04-20'
slug: []
categories:
  - R
tags:
  - spreadsheets
image:
  caption: ''
  focal_point: ''
---

This made me gasp🤯

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I&#39;ve seen prod taken down by someone adding a column and ruining a VLOOKUP.</p>&mdash; Too Lazy to Fail (@toolazytofail1) <a href="https://twitter.com/toolazytofail1/status/1516793435268730889?ref_src=twsrc%5Etfw">April 20, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 