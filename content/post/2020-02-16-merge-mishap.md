---
title: Merge Mishap!
author: kevin
date: '2020-02-16'
slug: merge-mishap
categories:
  - R
tags:
  - Git
image:
  caption: ''
  focal_point: ''
---

On recent Saturday morning, I ran into a merge conflict. The merge conflict was in data files that are updated nearly daily. Resolving the conflicts was not going to be worth the effort.

I searched on the git error message and determined to try a `git reset` command and this did the trick. 