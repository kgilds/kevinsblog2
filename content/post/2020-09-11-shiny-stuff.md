---
title: Shiny Stuff
author: Kevin
date: '2020-09-11'
slug: shiny-stuff
categories:
  - R
tags:
  - Shiny
image:
  caption: ''
  focal_point: ''
---

I have been in data collection mode and learning tricks in Qualtrics but here is some fun Shiny stuff I have this week.

A new Shiny Developer Series: https://www.youtube.com/watch?v=RcnfYcSsY8w

Here is the link to the [repo](https://github.com/rstudio/reactlog/) and [website](https://rstudio.github.io/reactlog/) 



The next resource is a presentation from a UseR! presentation: https://youtu.be/HFXOQVpWOFk
The presentation highlights [CanvasXpress](https://canvasxpress.org/index.html) 


I randomly found this [artlicle](https://plotly-r.com/improving-ggplotly.html) on dynamic tics with a ggploty(); I have a plot that does not do well with changing parameter and can't wait to try it.  
