---
title: Dashboard and Rds notes
author: Kevin
date: '2019-01-28'
slug: dashboard-and-rds-notes
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

> 4:00-6:00 am

This morning I had re-install R; apparently I failed this on Saturday. I had some challenges and have had to re-install all my packages. 

I discovered that this script stopped working and I had to make a change from save to write_rds. The error message I was getting was not a valid input. 


```r
save(object, file = ".rds") #incorrect

write_rds(object, file.path = ) #correct
```

I was able to get the Q1 Academic results in the boxes on the dashboard. I am happy I stuck with the correct process and made it dynamic. Speaking of dashboards; I discovered this [package](https://rinterface.github.io/shinydashboardPlus/). This seem awesome, if not overwhelming. 


I was able to play with page_dowm to render a report. So close but not quite ready for production yet. 



