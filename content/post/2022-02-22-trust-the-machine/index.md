---
title: The Machine Knows!
author: Kevin
date: '2022-02-22'
slug: []
categories:
  - R
tags:
  - WIC
image:
  caption: ''
  focal_point: ''
---



Here is an article to chew on @thomasnealley http://mcnamarafallacy.com/

The subtitle is _measurement is not understanding_

Can we find the balance between completely  data driven and going solely on gut? 

Have you seen this  _Office_ episode 

https://youtu.be/DOW_kPzY_JY

> The machine knows 




It is precarious not to ask questions of a measurement. This is the hard part!

Some questions: 

How does the the measurement fit your story of the situation?

How does this measurement contradict your understanding of the situation?

