---
title: Batch Files
author: Kevin
date: '2019-05-19'
slug: batch-files
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

Over the past year, I have shifted away from making large epic `Rmarkdown` files to sourcing scripts using `Rmarkdown` as a quasi compiler. I am not certain that this is a great use case; as I should be using `make` and or the `drake package`.  

My `Rmarkdown` hack had been working well until recently. It is becoming increasingly frustrating. 

See the issue thread
[Github Issue](https://github.com/rstudio/rstudio/issues/4462)


Ultimately, I want to use the [drake package](https://ropensci.github.io/drake/) but I am struggling to understand how to implement 



Hopefully these are helpful links to get me started

[stackoverflow](https://stackoverflow.com/questions/13994250/how-do-i-launch-multiple-batch-files-from-one-batch-file-with-dependency)

`CALL D:\jboss-5.1.0.GA-jdk6\jboss-5.1.0.GA\bin\run.bat
CALL batch1.bat
CALL batch2.bat
CALL batch3.bat`


[Task Scheduler](https://www.r-bloggers.com/how-to-run-r-from-the-task-scheduler/)

[Concurrently R scripts](https://stackoverflow.com/questions/43914512/run-multiple-r-scripts-concurrently-through-a-batch-file)

