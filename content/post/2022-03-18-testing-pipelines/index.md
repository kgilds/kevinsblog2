---
title: Testing Pipelines
author: Kevin
date: '2022-03-18'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


Finally got an automated test working with gitlab CI/CD runner. I was able to get it running using the template from the [gitlabr](https://statnmap.github.io/gitlabr/).  I trimmed the template down to just the build and test stage. The script below worked great

```
'covr::gitlab(quiet = FALSE)'

```



However, when I tried to get fancier with this

```
'covrpage::covrpage()'

```

This failed with this error message

 > Using bundled GitHub PAT. Please add your own PAT to the env var `GITHUB_PAT`
 
 That will be for tomorrow. 