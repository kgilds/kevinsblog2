---
title: Weekly List
author: Kevin
date: '2020-07-20'
slug: weekly-list
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


I was looking to host my own shiny-server and big reason is to host Rmd documents. 

This [tutorial](https://www.charlesbordet.com/en/guide-shiny-aws/#1-create-a-shiny-app) by Charles Bordet is very substantive and slings vast knowledge about DNS and protecting the Shiny Application with a password. I supplemented this tutorial with this [article](https://abndistro.com/post/2019/07/06/deploying-a-shiny-app-with-shiny-server-on-an-aws-ec2-instance/). The article shows how to grab an established Rstudio server and Shiny Server AWS instance. 


Another helpful [article](https://sharla.party/post/usethis-for-reporting/) I found last week is from Sharla Gelfand. The article details how to utilize the usethis package for basic reporting. I king of forgot about this and it also pointed me to the [assertthat](https://github.com/hadley/assertthat) package. This feel more manageable to try.  
