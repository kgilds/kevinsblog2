---
title: 'Coding '
author: Kevin
date: '2022-06-15'
slug: []
categories:
  - R
tags:
  - R
  - python
image:
  caption: ''
  focal_point: ''
---


I became better at using spreadsheets when I learned to code. I became better at using R when I learned to use Python. The concepts build on another. That is why language flame wars on social media are exhausting and seem to be calls for attention. 
