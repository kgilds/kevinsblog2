---
title: Late Blooming Bugs
author: Kevin
date: '2019-02-03'
slug: late-blooming-bugs
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

I ran into a bug from R scripts that have been running fine for 4-5 months. 

This script was breaking the code

``r
filter(finished == "true")
``

This fixed it; 
``r
filter(finished == "TRUE")
``
I am not sure why it did not break sooner.
