---
title: Select Statements
author: Kevin
date: '2022-04-30'
slug: []
categories:
  - SQL
tags: []
image:
  caption: ''
  focal_point: ''
---

Below is the structure of a SQL statement and notes from

    [^1]: Teate, Renee  **SQL for Data Scientists**

`SELECT` = The columns of interest. \* equals all. It is best practice to write out the columns of interest to avoid breaking data pipelines.

`FROM` = Schema.Table

`WHERE` = Conditional filter statements

`GROUP BY` = Columns to group on.

`ORDER BY` = Columns to sort on

`LIMIT` = Limit the number of rows returned to check


An exmaple. 

``` sql

SELECT 
	market_date,
	customer_id,
	vendor_id,
	ROUND(quantity * cost_to_customer.qty, 2) AS Price
	FROM farmers.market.customer_purchases
	LIMIT 10


```
