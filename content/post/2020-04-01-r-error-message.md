---
title: R error-message
author: kevin
date: '2020-04-01'
slug: r-error-message
categories:
  - R
tags:
  - error message
image:
  caption: ''
  focal_point: ''
---
 
Many times R error messages have a bigger bark than bite. They appear much worse than they actually are. Case in point this error message from this morning.

>  	cyclic namespace dependency detected 

This sounds awful coupled with the fact the error was bricking my package and shiny application.

However it was a quick fix from this [post](https://r.789695.n4.nabble.com/cyclic-namespace-dependency-detected-td4641312.html)

He had the same issue; an R script in the R directory of the package that called the package. 