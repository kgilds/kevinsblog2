---
title: More Glue
author: Kevin
date: '2022-05-22'
slug: []
categories:
  - R
tags:
  - Glue
  - file-path
image:
  caption: ''
  focal_point: ''
---


I am getting more comfortable with the [`glue package`](https://glue.tidyverse.org/)

File paths are tricky but it is amazing what you can do when you can figure out how to dynamically create file names and paths.

The script below lets me save the Excel workbook in the directory of the client_id. Now that I look at this I may make the file name dynamic as well.


```r
openxlsx::saveWorkbook(global, here::here("budgets", glue(
  '{bp_set$client_id}'), "updated-global.xlsx"), overwrite = TRUE)
```