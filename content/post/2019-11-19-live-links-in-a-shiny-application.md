---
title: Live Links in a Shiny Application
author: Kevin
date: '2019-11-19'
slug: live-links-in-a-shiny-application
categories:
  - R
tags:
  - Shiny
image:
  caption: ''
  focal_point: ''
---

I finally figured out how to make a list item dynamic in a Shiny Application. One of the items I wanted to display is a web link to the site location and wanted the link to be live to avoid copy and pasting.

This [stackoverflow post](https://stackoverflow.com/questions/30901027/convert-a-column-of-text-urls-into-active-hyperlinks-in-shiny) helped me get it working.

I also needed this [thread](https://groups.google.com/forum/#!topic/shiny-discuss/7C-mXRBe2mw) to apply the ```htmlOutput``` in my application. 
