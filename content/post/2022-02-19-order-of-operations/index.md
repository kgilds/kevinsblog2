---
title: 'Order of Operations '
author: Kevin
date: '2022-02-19'
slug: []
categories:
  - Docker
tags: []
image:
  caption: ''
  focal_point: ''
---

Order of operations–the order of your calculations matters when computing a math problem.

Order of operations also matter with computers as well. I had to do a near complete re-install of Pop-OS last week and had to re-install some programs. I could not get VS code to recognize Docker. I was searching and searching for what steps I missed and had a strong hunch is was about user permission.

I determined I had updated this in Docker after installing VS code. So I Uninstalled VS code and did a quick reinstall and now everything is working as expected.

Maybe this will bridge somewhere but I suspect it aligns with thinking like or communicating effectively with your computer.