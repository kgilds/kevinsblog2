---
title: inex.Rmd-indeed
author: kevin
date: '2020-04-12'
slug: inex-rmd-indeed
categories:
  - R
tags:
  - blogdown
image:
  caption: ''
  focal_point: ''
---

I was working to update blog posts and wanted to add featured images to the post. I have completed this prior but could not quite replicate the process. I finally changed the .Rmd file name to index.Rmd and whola--it works. 