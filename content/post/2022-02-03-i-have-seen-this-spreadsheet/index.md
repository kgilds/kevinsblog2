---
title: I have seen this spreadsheet
author: Kevin
date: '2022-02-03'
slug: []
categories:
  - R
tags: [WIC, spreadsheet]
image:
  caption: ''
  focal_point: ''
---


> Create a spreadsheet that breaks all the rules!

I write to show an example of chaos. 

Yesterday I wrote about best practices with [spreadsheets](https://kgilds.rbind.io/post/2022-02-02-spreadsheets/). When I finished up, I had a flashback trying to parse and make sense of a spreadsheet that defied all of the conventions.

The highlights of this spreadsheet. 

1. Colors conveyed an outcome

2. Dates were in many formats--of course none in  YYYY-MM-DD. 

3. My personal favorite is when someone puts more than one value in a cell.  


