---
title: Overlaying Density Plots
author: Kevin
date: '2018-12-29'
slug: overlaying-density-plots
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

A goal of mine was to create density plots that showed current achievement  with past achievement.



One thing I learned is that you need to explicitly specify the data argument in the plotting2 function call. A current challenge is the aesthetics are not quite correct. The color of the lines are mix matched. This [post](https://stackoverflow.com/questions/12199919/overlapped-density-plots-in-ggplot2) may help resolve
