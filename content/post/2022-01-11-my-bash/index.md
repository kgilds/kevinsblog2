---
title: My bash
author: Kevin
date: '2022-01-11'
slug: []
categories:
  - Automation
tags:
  - git
image:
  caption: ''
  focal_point: ''
---

I added a bash scrip to my pre-commit hook to automate the rendering of my README. Unfortunately I can't seem to get it to fire.

This is what the script looks like. 





```
#!/bin/bash
if [ README.Rmd -nt README.md ] || [ ! -f README.md ]
then
        R -e "rmarkdown::render('README.Rmd')"
        git add README.md
fi


README=($(git diff --cached --name-only | grep -Ei '^README\.[R]?md$'))
MSG="use 'git commit --no-verify' to override this check"

if [[ ${#README[@]} == 0 ]]; then
  exit 0
fi

if [[ README.Rmd -nt README.md ]]; then
  echo -e "README.md is out of date; please re-knit README.Rmd\n$MSG"
  exit 1
elif [[ ${#README[@]} -lt 2 ]]; then
  echo -e "README.Rmd and README.md should be both staged\n$MSG"
  exit 1
fi
if [ README.Rmd -nt README.md ] || [ ! -f README.md ]
then
        R -e "rmarkdown::render('README.Rmd')"
        git add README.md
fi
```

I think I just need to think about this. Maybe there is another hook to use. 