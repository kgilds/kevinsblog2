---
title: Assumptions
author: Kevin
date: '2022-02-25'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

Some advice from the [Data Mishap Night!](https://datamishapsnight.com/agenda-2022/)

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Wisdom from <a href="https://twitter.com/hmason?ref_src=twsrc%5Etfw">@hmason</a> :<br><br>If it works the first time: do not trust it<br><br>If it&#39;s too good too fast: do not trust it<br><br>Do not trust it<br><br> <a href="https://twitter.com/hashtag/DataMishapsNight?src=hash&amp;ref_src=twsrc%5Etfw">#DataMishapsNight</a></p>&mdash; Mike Mahoney (@MikeMahoney218) <a href="https://twitter.com/MikeMahoney218/status/1497016496433901569?ref_src=twsrc%5Etfw">February 25, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 