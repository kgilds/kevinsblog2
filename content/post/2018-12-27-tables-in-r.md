---
title: Tables in R
author: Kevin
date: '2018-12-27'
slug: tables-in-r
categories:
  - R
tags:
  - chromebook data science
header:
  caption: ''
  image: ''
---

I finished up a chapter on Data Tables today. I knew tables served a purpose but I love the opinionated view of tables in the course. I love that they applied the concept of an exploratory table to explanatory table. 

* Display should make comparison from top to bottom

* The order of rows should be logical

* The order of columns should be logical

1. The most important to the left

* Include informative of labels

* significant digits--2 digits are enough

* good title or caption

* source of the information


A great link on table design

[table design](https://design-nation.icons8.com/intro-to-data-tables-design-349f55861803)