---
title: Databases
author: Kevin
date: '2022-03-03'
slug: []
categories:
  - R
tags: [dbplyr]
image:
  caption: ''
  focal_point: ''
---


If you are going to work with data; you need to know how databases work or should. You can do a lot just knowing SQL. I know enough but would honestly use an [dbplyr](https://dbplyr.tidyverse.org/) R package to convert the script to SQL. I am hoping to dig into this package some more. 