---
title: Themese in ggplot2
author: Kevin
date: '2018-12-27'
slug: themese-in-ggplot2
categories:
  - R
tags:
  - chromebook data science
  - ggplot2
header:
  caption: ''
  image: ''
---

I finished up the modules Customization in plotting2 and saving plots. Learning about themes  filled in gap I was missing. In the saving plots module I discovered the ggsave function. I don't think I ever truly thought about saving the plot directly. 

Maybe as I get better at making plots--I will want to save them. 