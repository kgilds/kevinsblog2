---
title: Tables with Kable
author: Kevin
date: '2019-01-15'
slug: tables-with-kable
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

This morning, I forgot how to adjust the decimal length in my rendered table. 
This [link](https://www.rdocumentation.org/packages/knitr/versions/1.21/topics/kable) from the kable package. 

I need to use the digits argument from the kable package. 