---
title: SQL
author: Kevin
date: '2022-01-17'
slug: []
categories:
  - Data science
tags:
  - SQL
image:
  caption: ''
  focal_point: ''
---


<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Captions are done (though YouTube said it might be a little while before they post), and I put all of the links in the video description, so you can get set up to start writing MySQL queries with the sample farmer&#39;s market database today!<a href="https://t.co/jTiuR3CRnJ">https://t.co/jTiuR3CRnJ</a></p>&mdash; Data Science Renee (@BecomingDataSci) <a href="https://twitter.com/BecomingDataSci/status/1482801083944783873?ref_src=twsrc%5Etfw">January 16, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


This seems like a great resource. I know the basics of SQL but it is not part of my practice. 