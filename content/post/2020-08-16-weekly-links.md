---
title: Weekly-Links
author: Kevin
date: '2020-08-16'
slug: weekly-links
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

> Alternatively you could bury your documents in /inst/docs, since the inst folder is fair game-Miles McBain

I feel seen

This [post](https://www.milesmcbain.com/posts/an-okay-idea) made me think about my workflow. I take a package my project approach but am thinking about when there is a new cycle.


> Coding is not a linear journey: More about expanding toolset for the right job at the tight time--Thomas Mock


This [presentation deck](https://docs.google.com/presentation/d/e/2PACX-1vRo1eXJtiwo6aTA8KZ2E-bUbv2GOonC2RIVk_5eWQ5y-ADXbRamBhHaa3w1vMW6BkEPOMJ13ZahSo8Q/embed?start=false&loop=true&delayms=30000&slide=id.p) 


This [package](https://rtask.thinkr.fr/download-gitlab-or-github-issues-and-make-a-summary-report-of-your-commits/) seems fun. 