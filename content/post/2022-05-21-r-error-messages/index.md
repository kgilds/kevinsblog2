---
title: R error messages
author: Kevin
date: '2022-05-21'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

`R` tends to have uninformative error messages but you pick up patterns. This one threw me off track for a bit

Can't subset columns that don't exist. ✖ Columns `AE-1`, `AE-2`, `AE-3`, `AE-4`, `HR-3`, etc. don't exist

Folks what threw me off was that I was not trying to subset these columns and secondly these columns are not in the object I was working with.😰

I deduced that I was trying to call a variable that was not in the dataset and that was the problem.
