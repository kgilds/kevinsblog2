---
title: 'Episode 3: Shiny Developer Series'
author: Kevin
date: '2019-06-29'
slug: episode-3-shiny-developer-series
categories:
  - R
tags: [Shiny]
image:
  caption: ''
  focal_point: ''
---

This was an interesting episode and opened my eyes to some possibilities down the road.


<iframe width="560" height="315" src="https://www.youtube.com/embed/YBazPNSRhoc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>