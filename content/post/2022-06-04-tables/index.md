---
title: Tables
author: Kevin
date: '2022-06-04'
slug: []
categories:
  - R
tags: [glue]
image:
  caption: ''
  focal_point: ''
---

I am struggling to find a way to replicate an Excel sheet in a Rmarkdown document. I am going in circles with just placing values in table. A couple of questions?

Maybe the document does not have to replicate the Excel table blocks?

Maybe I can just use a regular html table and interest the values using glue? 