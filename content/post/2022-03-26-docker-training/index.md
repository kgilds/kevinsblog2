---
title: Docker Training
author: Kevin
date: '2022-03-26'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


Here is hoping there will be a remote option for this or at least posted online.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Docker + R = ❤️<br><br>I am super excited to be part of the UseR! 2022 conference, going to run a workshop about Docker for R users! <br><br>More details: <a href="https://t.co/0tusJHuIYH">https://t.co/0tusJHuIYH</a><a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> <a href="https://twitter.com/hashtag/Docker?src=hash&amp;ref_src=twsrc%5Etfw">#Docker</a> <a href="https://t.co/J3iqcaWXIY">pic.twitter.com/J3iqcaWXIY</a></p>&mdash; Rami Krispin (@Rami_Krispin) <a href="https://twitter.com/Rami_Krispin/status/1506625856143208451?ref_src=twsrc%5Etfw">March 23, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 