---
title: Smart
author: Kevin
date: '2022-06-10'
slug: []
categories:
  - management
tags: []
image:
  caption: ''
  focal_point: ''
---


I liked this post from Seth the other day https://seths.blog/2022/06/are-you-smart/

* Situational Awareness
* Filtering Information
* Troubleshooting
* Good Taste
* Empathy and compassion for others
* The ability to make decisions that further your goals

I am all in on these items but would like to think additionally about good taste and the ability to make decisions that further your goal.s.

