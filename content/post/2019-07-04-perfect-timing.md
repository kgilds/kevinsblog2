---
title: Perfect-timing
author: Kevin
date: '2019-07-04'
slug: perfect-timing
categories:
  - R
tags: [R-lang]
image:
  caption: ''
  focal_point: ''
---

`rlang 4.0`  was released recently and it has made a big impact on my ability to create functions that use a variable from a date frame.  I struggled with the previous method of using ~!!enquo~ and I never implemented with this method. 

Using the new `{{}}` I was able write new functions with directly using variable names as arguments. 



https://www.tidyverse.org/articles/2019/06/rlang-0-4-0/

https://www.tidyverse.org/articles/2019/06/rlang-0-4-0/



Previously, I was hard wiring my functions; now I can abstract to another layer.

Here is the release [update](https://www.tidyverse.org/articles/2019/06/rlang-0-4-0/)
