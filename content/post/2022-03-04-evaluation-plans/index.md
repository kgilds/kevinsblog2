---
title: Evaluation Plans
author: Kevin
date: '2022-03-04'
slug: []
categories:
  - Evaluation
tags: []
image:
  caption: ''
  focal_point: ''
---


Nothing more to say or add 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">An evaluation plan (imagined) and then written by a grant writer (who is not necessarily an evaluator or a researcher and/or doesn’t work with performance measures and data on a daily basis) poses a very difficult challenge to the evaluator who is “given” the project to evaluate.</p>&mdash; Kavita Mittapalli, Ph.D. (@KavitaMNA) <a href="https://twitter.com/KavitaMNA/status/1499735024635191298?ref_src=twsrc%5Etfw">March 4, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 