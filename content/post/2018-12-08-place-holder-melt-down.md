---
title: Place Holder Melt Down
author: Kevin
date: '2018-12-08'
slug: place-holder-melt-down
categories:
  - R
tags:
  - Shiny
  - WorkNotes
header:
  caption: ''
  image: ''
---

I was working to update a Shiny  Dashboard this morning and could not get a page to render. I thought why not render a duplicate table as place holder. This was a huge fail  as all the data disappeared from all the other tables. It turns out I started a new page within a row of another page.
