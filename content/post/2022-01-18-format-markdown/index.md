---
title: Formating Markdown
author: Kevin
date: '2022-01-18'
slug: []
categories:
  - Notes
tags:
  - Rmarkdown
image:
  caption: ''
  focal_point: ''
---

> 2 spaces gives you a new line in markdown. 

I was drafting a letter with [`pagedown`](https://pagedown.rbind.io/#letter) package and formatting issues with the address block. 

The `To` address field was running all on the same line. 


This [answer](https://stackoverflow.com/questions/34735772/markup-for-an-email-signature-or-postal-address-in-markdown#34748652) saved me tons of time. 

