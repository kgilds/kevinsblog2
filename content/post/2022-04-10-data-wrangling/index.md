---
title: 'Data Wrangling '
author: Kevin
date: '2022-04-10'
slug: []
categories:
  - R
tags:
  - dplyr
  - data management
  - dupes
image:
  caption: ''
  focal_point: ''
---

I am in a continuous battle against duplicate entries. Last week, I found 9 entries that needed to be removed. It is an easy fix but tedious 👾 . My mental model was to try and use the `%in%` operator. My challenge was that I did not want these particular values--I wanted to exclude them.

This [post](https://stackoverflow.com/questions/5831794/opposite-of-in-exclude-rows-with-values-specified-in-a-vector) helped me figure it out and below is the code that I used. What surprised me was putting `!` operator in front of the variable. But it worked, and I did not have list out 9 separate lines to remove the entries.

``` r
dplyr::filter(!response_id %in%  issue_91_r)
```
