---
title: Tuesday Links
author: Kevin
date: '2019-09-11'
slug: tuesday-links
categories:
  - R
tags: [reporting]
image:
  caption: ''
  focal_point: ''
---

A goal of mine is step up the appearance of my reports this year.

This morning started with this jem on [building note boxes](http://desiree.rbind.io/post/2019/making-tip-boxes-with-bookdown-and-rmarkdown/). 

Links, I found tonight. 

[Reports as Functions](https://daranzolin.github.io/2019-09-03-reports-as-functions/) at [R-weekly](https://rweekly.org/) and that post led me to a post on building a [report template](http://ismayc.github.io/ecots2k16/template_pkg/) in Rmarkdown.



