---
title: Run Time Shiny
author: 'Kevin '
date: '2022-05-20'
slug: []
categories:
  - R
tags:
  - shiny, rmarkdown
image:
  caption: ''
  focal_point: ''
---

A video on using a Rmarkdown document as a Shiny Document to help make the document dynamic.  https://www.youtube.com/watch?v=fgOw78Sp8TA