---
title: Dynamic Reporting with Shiny
author: Kevin
date: '2022-03-15'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


I attended a [R-Ladies St. Louis Meetup](https://github.com/winterstat/RLadiesSTL-shinyreports) tonight and happy I did. The topic was on Dynamic Reporting with a Shiny Application. I do this with my apps and was hoping to pick some extra tips. I did and more… the fun part is when the presenter demonstrates a topic you have an open issue on!

