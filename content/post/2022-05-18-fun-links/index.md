---
title: Fun Links
author: Kevin
date: '2022-05-18'
slug: []
categories:
  - R
tags: [knitr, Rmarkdown]
image:
  caption: ''
  focal_point: ''
---


Again always check the data type–this time it did not take as long to check the data type.

This is a good article on Rmarkdown https://www.rstudio.com/blog/r-markdown-tips-and-tricks-3-time-savers/
Interesting functions found here:

knitr::spin()
knitr::purl()

I also liked the section on creating an Appendix with

# Appendix

```{r}
#| ref.label=knitr::all_labels(),
#| echo = TRUE,
#| eval = FALSE
