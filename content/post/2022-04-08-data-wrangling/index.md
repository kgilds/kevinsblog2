---
title: Data Wrangling
author: Kevin
date: '2022-04-08'
slug: []
categories:
  - R
tags:
  - dplyr
image:
  caption: ''
  focal_point: ''
---


I saw this morning <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Lovely pattern in R is if I have a target column, and I want to find out how each of the other columns correlates with the target:<br><br>data %&gt;%<br>pivot_longer(-target) %&gt;% <br>group_by(name) %&gt;%<br>summarise(correlation = cor(target, value))</p>&mdash; David Neuzerling (@mdneuzerling) <a href="https://twitter.com/mdneuzerling/status/1512261950054436875?ref_src=twsrc%5Etfw">April 8, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

and it led me here 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I prefer:<br><br>data %&gt;% <br> summarise(across(-target, ~cor(target, .x)))</p>&mdash; Bryan Shalloway (@brshallo) <a href="https://twitter.com/brshallo/status/1512293448371507208?ref_src=twsrc%5Etfw">April 8, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 