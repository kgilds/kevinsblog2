---
title: NA Values
author: Kevin
date: '2022-03-27'
slug: []
categories:
  - R
tags: [naniar, wic]
image:
  caption: ''
  focal_point: ''
---


The book [_R for Data Science_](https://r4ds.had.co.nz/index.html) book starts with data visualization and it makes more and more sense all the time. Data Visualization is critical in the data exploration phase.

This morning I was into the thick of data exploring, and I knew the data set I was working with used -99 as a placeholder of NA values. I converted those -99 to NA and when I created visualization much much to my surprise there were additional nonsensical negative numbers in the data set. Happy to have found out early rather than later! I used the [`naniar` package](https://cran.r-project.org/web/packages/naniar/vignettes/replace-with-na.html) to make the conversion and it has a very good vignette.  