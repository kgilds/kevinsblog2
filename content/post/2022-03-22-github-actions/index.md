---
title: Github Actions
author: Kevin
date: '2022-03-22'
slug: []
categories:
  - R
tags:
  - shiny
  - githubactions
image:
  caption: ''
  focal_point: ''
---

I would love to deploy my Shiny Application from Github...I think.  Check this out https://github.com/r-lib/actions/blob/v2/examples/shiny-deploy.yaml