---
title: Weekly Links & Notes
author: Kevin
date: '2020-07-04'
slug: weekly-links-notes
categories:
  - r
tags:
  - Vim, git
image:
  caption: ''
  focal_point: ''
---

I learned some important lessons with the vim editor and git this week.


> Help I need an adult!

When I find myself in the **vim editor**. Thankfully, I found some a good resource to gracefully handle these encounters. 

* select i from the keyboard to enter into the Insert Mode

* write commit message 

* select the escape key to exit Insert Mode

* select :x to save and escape. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ebZzVAZC7tc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Hopefully this tutorial helps with merge conflicts as well. 

The other key learning this week consisted of a change in git workflow.

This [book](https://git-scm.com/book/en/v2) is my favorite reference for learning git. 

I now merge the main branch into the development branch after changes. 

This always made me nervous. I had been hesitant to mange my git branches-- I start a new branch if I do not finish working on a development branch. This had been limiting. I managed to pull it off this week with no trouble.



