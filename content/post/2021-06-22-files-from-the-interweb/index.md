---
title: Files from the interweb
author: Kevin
date: '2021-06-22'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

One thing to recall; when I pull data from the web like the Florida Department of Education I need to save the file output. One day they may move the file elsewhere. 