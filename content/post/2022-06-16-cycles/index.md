---
title: Cycles
author: Kevin
date: '2022-06-16'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

I am not database professional but social service manager.
I get access to databases at my day job–responsibility for sure but it has created virtuous cycle of learning more and creating more value.