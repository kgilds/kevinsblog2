---
title: Shinyhelper with Golem
author: Kevin
date: '2019-10-27'
slug: shinyhelper-with-golem
categories:
  - R
tags:
  - Shinyhelper
  - Golem
image:
  caption: ''
  focal_point: ''
---

I had a good use case to use [Shinyhelper](https://github.com/cwthom/shinyhelper) in a dashboard I am building. I build my shiny dashboards with [Golem](https://github.com/ThinkR-open/golem). I was putting the observe_event in the server module.  

The key to making it work was to put the observe_helpers function in the main app_server.R file.
