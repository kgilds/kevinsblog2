---
title: Active Voice
author: Kevin
date: '2022-01-23'
slug: []
categories:
  - Writing
tags: [Writing]
image:
  caption: ''
  focal_point: ''
---

Good writing is hard and I  want to get better. I found this [technical writing course](https://developers.google.com/tech-writing/one/active-voice) from Google

Here is the formula for active and passive voice sentence structures provided in the course. 

> Active Voice Sentence = actor + verb + target


> Passive Voice Sentence = target + verb + actor

Now,  I have a way to visualize the pattern. I may need to keep this in view at all times. 