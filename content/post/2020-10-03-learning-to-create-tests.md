---
title: Learning to create tests
author: Kevin
date: '2020-10-03'
slug: learning-to-create-tests
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

I hit a benchmark this week by bearing down and learning to generate automatic tests for my Shiny Application. As [Hadley](https://r-pkgs.org/tests.html) points out I was testing informally by doing some quick math in my head prior to pushing updates to Shinyappsio. 

My biggest block and I am not sure why is that you are just writing code within a function and live testing the output. 

Below is the code output that replaces my mental math. The function output should always be 5 times the number of row of the pre_program_data data frame.

```
test_that("pre_program_data should 5 times number of rows of pre_program_data ", {
  pre_results <- gswcf::pre_outcomes(pre_program_data)
  expect_equal(nrow(pre_results), dim(pre_program_data)[1]*5)
  
})
```