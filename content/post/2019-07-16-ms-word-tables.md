---
title: MS Word Tables
author: Kevin
date: '2019-07-16'
slug: ms-word-tables
categories:
  - R
tags: ["kableExtra"]
image:
  caption: ''
  focal_point: ''
---

My  tables were not rendering from Rmarkdown to MS word in a satisfactory manner. 

I took a breath and did a goggle search and found this [issue](https://github.com/haozhu233/kableExtra/issues/308)
from `kableExtra`. I had to terminate kableExtra and use plain kable function to ensure the tables could render in MS Word. 
