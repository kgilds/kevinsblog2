---
title: package-down
author: Kevin
date: '2019-09-08'
slug: package-down
categories:
  - R
tags: [pkg-down]
image:
  caption: ''
  focal_point: ''
---

I was hesitant to try [package down](https://github.com/r-lib/pkgdown) it seemed like it was going to be difficult. However, I found it surprisingly easy to get the basic template up and running and easy to publish with gitlab and [netlify](https://www.netlify.com/).

My only challenge was customizing the reference page. I mainly needed to slow down and be more careful with my yaml file. I used this [repo](https://github.com/jranke/mkin/blob/master/_pkgdown.yml) to help me figure it out how to implement ~_pkgdown.yml~ in my directory; I like how they split the function list and made the navbar Functions/Data. 


