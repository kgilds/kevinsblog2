---
title: Shiny Modules
author: Kevin
date: '2019-04-17'
slug: shiny-modules
categories:
  - R
tags: ["Shiny"]
image:
  caption: ''
  focal_point: ''
---

I am thinking of trying to use Shiny Modules again. Here is the [link](https://shiny.rstudio.com/articles/communicate-bet-modules.html) to articles; I had a difficult time with these examples in the past; hopefully a fresh start will help. 

