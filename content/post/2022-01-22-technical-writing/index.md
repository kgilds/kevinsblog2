---
title: Technical Writing
author: Kevin
date: '2022-01-22'
slug: []
categories:
  - Communication
tags:
  - Communication
image:
  caption: ''
  focal_point: ''
---

I am in the midst of writing reports and technical writing is on my mind. This Twitter thread on technical  writing sparked my interest.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I&#39;ve heard Google offers some classes that are really good and definitely wanted to check them out. Might be these (not 100% sure) <a href="https://t.co/K9KsKMYcAy">https://t.co/K9KsKMYcAy</a></p>&mdash; Jamund Ferguson (@xjamundx) <a href="https://twitter.com/xjamundx/status/1484937768161529857?ref_src=twsrc%5Etfw">January 22, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


And the [classes](https://developers.google.com/tech-writing) from Google look pretty good.  


<blockquote class="twitter-tweet"><p lang="en" dir="ltr"><a href="https://t.co/fRjjGIdPUF">https://t.co/fRjjGIdPUF</a> isn&#39;t about technical writing per se, but is about clear communication. Williams taught at U. Chicago. The book is structured for course use.</p>&mdash; Dave W Smith (@dws) <a href="https://twitter.com/dws/status/1484991180194521090?ref_src=twsrc%5Etfw">January 22, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


<blockquote class="twitter-tweet"><p lang="en" dir="ltr"><a href="https://t.co/cR2gwBPxOv">https://t.co/cR2gwBPxOv</a> is always helpful, for any genre of writing.<br><br>I see that there is an Elements if Technical Writing, dunno how well it complements the Strunk and White classic.</p>&mdash; Jon Udell (@judell) <a href="https://twitter.com/judell/status/1484933683106619392?ref_src=twsrc%5Etfw">January 22, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 