---
title: R-Packages & Strings
author: Kevin
date: '2018-11-13'
slug: r-packages-strings
categories:
  - R
tags:
  - Stringr
header:
  caption: ''
  image: ''
---

Working on updating my R-package this morning and I had to remind my self how to make the update.

Rinse and repeat

```
devtools::document()

```

```
devtools::check()

```

On a unrelated note I am stuck with string problem.


This is producing strange output. 



```
status_report_site$site <- gsub("FREEDOM", "FREEDOM MIDDLE", fixed=FALSE, status_report_site$site)

status_report_site$site <- gsub("FREEDOM MIDDLE  SCHOOL", "FREEDOM MIDDLE", fixed=FALSE, status_report_site$site)

```

For instance, I want to change from Freedom Middle School to Freedom Middle but the output is producing Freedom Middle Middle School. 


