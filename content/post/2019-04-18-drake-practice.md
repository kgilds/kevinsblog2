---
title: Drake Practice
author: Kevin
date: '2019-04-18'
slug: drake-practice
categories:
  - R
tags: ["DRAKE"]
image:
  caption: ''
  focal_point: ''
---

I have been working with the [drake package](https://ropensci.github.io/drake/). Last Sunday, I was able to make some progress; however when I run a plan; I anticipate more targets than what drake produces. 

The side benefit is that I see many opportunities to create functions to reduce lines of scripts. 
