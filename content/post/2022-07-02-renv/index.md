---
title: Renv
author: Kevin
date: '2022-07-02'
slug: []
categories:
  - R
tags:
  - linux
image:
  caption: ''
  focal_point: ''
---


My goodness my last post was on a system re-install and I feel I have been playing catch up ever since.

After the re-boot, I was still experiencing some package issues even wtih the renv package. However, I discovered this function call

```r
renv::install("package", rebuild = TRUE)

```

I had to reinstall and  rebuild some of the packages.

I need to keep trying to understand this package some more https://rstudio.github.io/renv/articles/renv.html. I am trying to understand the section on the global caches.

