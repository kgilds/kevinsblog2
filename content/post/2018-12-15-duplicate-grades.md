---
title: Trade offs
author: Kevin
date: '2018-12-15'
slug: duplicate-grades
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---




> 4:57-6:05 am:

Working through language arts grades data frame and there are more grades then students. I put the data frame through the aggregate function to get the mean language arts grade for each student. The trade off is that I can't get the summary by all the variables. 

```
aggreage