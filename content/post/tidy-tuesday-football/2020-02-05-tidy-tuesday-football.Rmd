---
title: tidy-tuesday-football
author: kevin
date: '2020-02-05'
slug: tidy-tuesday-football
categories:
  - R
tags:
  - TidyTuesday
image:
  caption: ''
  focal_point: ''
---


```{r}
library(tidyverse)
library(viridis)
```



```{r read-data}
attendance <- readr::read_csv('https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2020/2020-02-04/attendance.csv')
standings <- readr::read_csv('https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2020/2020-02-04/standings.csv')
games <- readr::read_csv('https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2020/2020-02-04/games.csv')

```

```{r}
df <- dplyr::left_join(attendance, standings, games, by = c("year", "team_name", "team"))
#df_1 <- dplyr::left_join(df,games, by = c("year", "team_name", "team"))

```


```{r}
names(df)
```

```{r}
glimpse(df)
```


```{r}
points_diff <- df %>%
  dplyr::group_by(year, team) %>%
  dplyr::summarise(median = median(points_differential, na.rm = TRUE))

points_diff

sd(df$points_differential)
```

```{r}
ggplot2::ggplot(df, aes(total, points_differential, color = playoffs)) +
  geom_point()
```



```{r tampa}
tampa <- df %>%
  dplyr:: filter(team == "Tampa Bay") %>%
  ggplot(., aes(y = points_differential, x = year, color = wins)) +
  geom_point() +
  scale_fill_viridis(discrete = TRUE, alpha=0.6) +
  theme_bw()

tampa
```

Question: Is point differential associated with winning and playoff appearance in Professional Football.  


```{r point-differential}
ggplot(df, aes(y = wins, x = points_differential, color = playoffs)) +
  geom_point() +
  scale_fill_viridis(discrete = TRUE, alpha=0.6) +
  theme_light() +
  labs(title = "Relationship between wins and point differential",
       caption = "Data from Pro Football Reference  \n tidytuesday \n @kevin_gilds \n  ") +
  ylab("Number of Wins") +
  xlab("Point Differential") +
  theme(axis.text = element_text(size = 14), 
                title = element_text(size = 12), 
                legend.position="right", 
                panel.grid.major = element_blank(), 
                panel.grid.minor = element_blank(),
                plot.caption = element_text(face = "italic", size = 8)
                
  )
```

A point differential of 100 or more is associated with a winning season and a playoff appearance.
