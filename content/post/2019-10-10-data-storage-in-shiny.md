---
title: Data Storage in Shiny
author: Kevin
date: '2019-10-10'
slug: data-storage-in-shiny
categories:
  - R
tags:
  - Shiny
image:
  caption: ''
  focal_point: ''

yml_blogdown_opts: draft  
---


Received a request for a Shiny Application that will process incoming data. I have a couple of applications that run locally but was not sure if I could use this with a Shiny Apps io. It turns out I probably can. This [post](https://shiny.rstudio.com/articles/persistent-data-storage.html) provides examples. Two good possiblities include Google Sheets and [Mongo DB](https://mlab.com/plans/pricing/)

