---
title: More tables
author: Kevin
date: '2022-06-13'
slug: []
categories:
  - R
tags:
  - gt
  - wic
image:
  caption: ''
  focal_point: ''
---


One advantage of coding is creating beautiful, informative and fun tables to present data

I love using the gt package for producing tables in R. Here are some updates
https://www.rstudio.com/blog/changes-for-the-better-in-gt-0-6-0/