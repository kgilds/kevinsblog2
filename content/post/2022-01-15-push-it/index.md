---
title: Push it!
author: Kevin
date: '2022-01-15'
slug: []
categories:
  - R
tags:
  - Git
image:
  caption: ''
  focal_point: ''
---


My computer was having some issues this morning and it hit me that I had not been committing and pushing updates of my `Rmarkdown` files to the remote. Thankfully, I was able to update and push my repo. 

I had my backup plan in place in the event of the worse but how awful would it have been to clone a non updated repo. Yikes!

A big sigh of relief that was only a wakeup call to keep pushing. 