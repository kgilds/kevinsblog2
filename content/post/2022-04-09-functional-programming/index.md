---
title: Functional Programming
author: Kevin
date: '2022-04-09'
slug: []
categories:
  - R
tags:
  - purrr
image:
  caption: ''
  focal_point: ''
---

I spent a good deal of time working through [Dynamic Filterer in Mastering Shiny](https://mastering-shiny.org/action-dynamic.html#dynamic-filter)


Below is a generic function to make inputs and server calls to dynamic filter values. 



```r
make_ui <- function(x, var) {
  if (is.numeric(x)) {
    rng <- range(x, na.rm = TRUE)
    sliderInput(var, var, min = rng[1], max = rng[2], value = rng)
  } else if (is.factor(x)) {
    levs <- levels(x)
    selectInput(var, var, choices = levs, selected = levs, multiple = TRUE)
  } else {
    # Not supported
    NULL
  }
}

```

```r
filter_var <- function(x, val) {
  if (is.numeric(x)) {
    !is.na(x) & x >= val[1] & x <= val[2]
  } else if (is.factor(x)) {
    x %in% val
  } else {
    # No control, so don't filter
    TRUE
  }
}

```


```r

ui <- fluidPage(
  sidebarLayout(
    sidebarPanel(
      make_ui(iris$Sepal.Length, "Sepal.Length"),
      make_ui(iris$Sepal.Width, "Sepal.Width"),
      make_ui(iris$Species, "Species")
    ),
    mainPanel(
      tableOutput("data")
    )
  )
)
server <- function(input, output, session) {
  selected <- reactive({
    filter_var(iris$Sepal.Length, input$Sepal.Length) &
      filter_var(iris$Sepal.Width, input$Sepal.Width) &
      filter_var(iris$Species, input$Species)
  })
  
  output$data <- renderTable(head(iris[selected(), ], 12))
}

```


I was able to apply the concepts/code above to the data frame I was using. I had some trouble but my next step is truly identifying what I want to display and communicate. 


Below is a nice intro video to the `purr` package. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/bzUmK0Y07ck" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>