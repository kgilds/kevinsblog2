---
title: Some Shiny Functions in bs4Dash
author: Kevin
date: '2020-12-15'
slug: some-shiny-functions-in-bs4dash
categories:
  - R
tags: [shiny]
image:
  caption: ''
  focal_point: ''
---

I enjoy using the [BS4DASH](https://rinterface.github.io/bs4Dash/) package and finally used the [```descriptionBlock function```](https://rinterface.github.io/bs4Dash/reference/descriptionBlock.html). This UI element is useful for displaying statistics. 


Another fun discovery is that [```bs4InfoBox``](https://rinterface.github.io/bs4Dash/reference/bs4InfoBox.html) can link to other pages in a dashboard.
Use this form 
```
tabName = "mod_home_ui",
title = "Interesing",
value = "75"
icon = "Intersting Icon",
```