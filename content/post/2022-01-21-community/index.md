---
title: Community
author: Kevin
date: '2022-01-21'
slug: []
categories:
  - Community
tags: []
image:
  caption: ''
  focal_point: ''
---


Here is a [post](https://counting.substack.com/p/making-room-for-mistakes?utm_campaign=post&utm_medium=email) from Randy Au about the value of data professional talking about the mistakes they have made.  He highlights the [Data Mishaps Night](https://datamishapsnight.com/) event and that data folks often run solo. Thus it is beneficial to hear that many folks encounter similar issues and make mistakes.

The same is true in the evaluation space. On the 3rd Friday of the month, I participate in a Zoom chat to discuss evaluation in the nonprofit space. It is always a highlight of my month where we discuss challenges and triumphs. 