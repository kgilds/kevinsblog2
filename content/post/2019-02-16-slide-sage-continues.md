---
title: Slide Sage Continues..............
author: Kevin
date: '2019-02-16'
slug: slide-sage-continues
categories:
  - R
tags: 
  - WorkNotes
header:
  caption: ''
  image: ''
---

> 5:15 am  - 6:00 am

I worked on io_slides and was able to bring the size of the tables under control with an argument with the kable package font_size. This worked well but when I got greedy and tried another output format it broke the tables. 

