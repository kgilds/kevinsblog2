---
title: 'More than coding '
author: Kevin
date: '2019-01-25'
slug: more-than-coding
categories:
  - podcast
tags:
  - podcast
header:
  caption: ''
  image: ''
---

This was an interesting [podcast](https://megaphone.link/VMP8036354183) that I listened to this morning. Key takeaways. Be a producer of content rather than merely a consumer of content on the internet. I had fun with my 9 year old doing some of the [coding exercises](https://world.kano.me/coding-challenges/training)

