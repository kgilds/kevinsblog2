---
title: Reports with Pagedown
author: kevin
date: '2020-02-24'
slug: reports-with-pagedown
categories:
  - R
tags:
  - pagedown
image:
  caption: ''
  focal_point: ''
---

I dislike writing and submitting technical reports in MS Word. I tested the pagedown template Chapman & Hall/CRC and I very much like the output. 

I had some concern when I downloaded the most recent version of pagedown and did not see the template for CRC when I selected new Rmarkdown document. 

It was an easy workaround as I just added it in the yaml section like this 

`output: pagedown::book_crc`

This [Git Repo](https://github.com/tvroylandt/sgdf_pagedown) seems like a good starting point in learning how to customize and tweak some output. 




