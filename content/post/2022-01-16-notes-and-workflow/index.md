---
title: Notes and Workflow
author: Kevin
date: '2022-01-16'
slug: []
categories:
  - workflow
tags:
  - notes
  - workflow
  - git
image:
  caption: ''
  focal_point: ''
---


I have been determined to blog daily for the sake of habit. I worry about the quality of the content but but some content is greater than no content. This thread gives me some encouragement.



<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Want to know the secret to blogging more often?<br><br>Lower your standards!<br><br>A post which you don&#39;t think is ready yet is a LOT better than a giant folder full of drafts that no-one ever gets to see<br><br>(Your readers won&#39;t ever know how good the thing you wanted to write would have been)</p>&mdash; Simon Willison (@simonw) <a href="https://twitter.com/simonw/status/1481300837154828294?ref_src=twsrc%5Etfw">January 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

I am hoping for long term gain with some temporary lower quality posts. That is I am hoping that I get better because I am writing everyday.  


As a bonus, I found this [blog post](https://simonwillison.net/2022/Jan/12/how-i-build-a-feature/) in the twitter thread.  I think this fits into my ideas about design and program evaluation. I have been working to document everything as an issue. On a related note, I took interest in this [post](https://github.com/readme/guides/private-documentation) on notes in the coding environment. 

