---
title: Still the same
author: Kevin
date: '2022-03-25'
slug: []
categories:
  - R
tags:
  - Data Science
  - WIC
image:
  caption: ''
  focal_point: ''
---


I keep seeing this theme of SQL

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">‣ Tackle projects in standalone and reproducible chunks.<br>‣ Brush up on file management–be able to programmatically navigate directories and read/write files.<br>‣ Get used to working in the cloud and what that means for managing libraries/environments.</p>&mdash; Mike Kearney📊🇺🇦 (@kearneymw) <a href="https://twitter.com/kearneymw/status/1507337390729449497?ref_src=twsrc%5Etfw">March 25, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

