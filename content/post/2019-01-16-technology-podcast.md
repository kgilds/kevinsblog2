---
title: Technology Podcast
author: Kevin
date: '2019-01-16'
slug: technology-podcast
categories:
  - podcast
tags:
  - podcast
header:
  caption: ''
  image: ''
---

I discovered this [podcast](https://glitch.com/culture/function/) from Anil Dash. The best part was looking up the podcast link and finding [Glitch](https://glitch.com/). This website seems like a lot of fun. 

