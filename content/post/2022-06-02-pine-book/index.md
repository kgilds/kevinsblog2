---
title: Pine Book
author: Kevin
date: '2022-06-02'
slug: []
categories:
  - Linux
tags: []
image:
  caption: ''
  focal_point: ''
---


Hope I can get one

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The PineBook Pro is a $219 Linux laptop with a 14 inch FHD IPS LCD display, a Rockchip RK3399 processor, 4GB of RAM and 64GB of eMMC storage. It&#39;s been out of stock for a year due to supply chain issues, but it will be available again by July, 2022. <a href="https://t.co/HQgstfYdLK">https://t.co/HQgstfYdLK</a> <a href="https://t.co/POOyCGnaFq">pic.twitter.com/POOyCGnaFq</a></p>&mdash; Liliputing (@liliputingnews) <a href="https://twitter.com/liliputingnews/status/1531657365484933126?ref_src=twsrc%5Etfw">May 31, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 