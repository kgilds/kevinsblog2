---
title: replace_na
author: Kevin
date: '2020-08-31'
slug: replace-na
categories:
  - R
tags: [tidyr]
image:
  caption: ''
  focal_point: ''
---

I tend to forget that the function replace_na takes the following form.

`don't_forget <- replace_na(list(pre_mean = "DQ", post_mean = "DQ"))`

Here is the documentation with examples to the  [function reference](https://tidyr.tidyverse.org/reference/replace_na.html)


