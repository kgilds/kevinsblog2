---
title: Data Analysis and Design
author: Kevin
date: '2022-03-16'
slug: []
categories:
  - R
tags:
  - data management
image:
  caption: ''
  focal_point: ''
---

> Design Matters

I came across this [article](https://streamlinedatascience.io/articles/a-synthesis-of-data-management-issues) today and enjoyed the terms to describe the issues.

A key word that stand out to me in the post is standardization. There are great points about consistent variables names, file types, variable formats. 

I know the trouble spots in my pipelines are when I allow free text entry. Free text entry is fine and welcome for comments but not when you need consistency.

I strive to make data entry forms a win/win for the person recording the data and me as the coder. 