---
title: Email with R
author: Kevin
date: '2022-02-27'
slug: []
categories:
  - R
tags: [blastula]
image:
  caption: ''
  focal_point: ''
---

I finally dug into the `blastula` package this weekend. For some reason, it was not sinking in with me. However, it started clicking today, and I wrote up some templates for future use. I really love the output of the messages. 
