---
title: The relative path
author: Kevin
date: '2019-05-23'
slug: the-relative-path
categories:
  - R
tags:
  - batch
  - R-Package
image:
  caption: ''
  focal_point: ''
---

I worked on creating a bat file to run R scripts this morning using this [post](https://kgilds.rbind.io/post/scheduled-tasks/). It was working until the process came across a relative path. 

Ok, this was my sign to make my analysis a package. 

I have been following the process with `usethis::package. My only confusion is connecting the Github Repo because I am starting the project locally first.