---
title: Tools for Packages
author: Kevin
date: '2019-07-19'
slug: tools-for-packages
categories:
  - R
tags: ["packages"]
image:
  caption: ''
  focal_point: ''
---

This is a great [post](https://rtask.thinkr.fr/blog/rmd-first-when-development-starts-with-documentation/) on building packages. Many good reference points in the post but I also like the idea of documenting in a Rmarkdown file. Spell check anyone. 