---
title: Shiny Fun
author: Kevin
date: '2022-04-16'
slug: []
categories:
  - R
tags:
  - shiny
image:
  caption: ''
  focal_point: ''
---

I took a couple of pointers from his episode of the [Shiny Developer Series](https://youtu.be/GdxVtl3FwmI)

1.  Manipulating strings from [stringr](https://stringr.tidyverse.org/) 📦 within from [reactable](https://glin.github.io/reactable/). There seemed to be good examples in this application.

2.  Pushing a development version a shiny appliction to shinyappsio. My goodness--I need to try this.
