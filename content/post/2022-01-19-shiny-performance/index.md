---
title: Shiny Performance
author: Kevin
date: '2022-01-19'
slug: []
categories:
  - R
tags: [Shiny]
image:
  caption: ''
  focal_point: ''
---

During the [_Mastering Shiny_](https://mastering-shiny.org/performance.html) bookclub there was a presentation on the [`profvis`](https://rstudio.github.io/profvis/examples.html#example-3---profiling-a-shiny-application). Here is more information from [Engineering Production Grade Shiny Apps](https://engineering-shiny.org/need-for-optimization.html?q=prof#profiling-shiny)