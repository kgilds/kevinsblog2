---
title: Pipe Control
author: Kevin
date: '2022-01-13'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

> I write to remember stuff!

I love using keyboard shortcuts but for the life of me can't recall the pipe operator. It is `Ctr-Shift-M`

Hopefully, it sticks!