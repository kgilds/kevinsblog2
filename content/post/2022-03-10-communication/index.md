---
title: Communication
author: 'Kevin '
date: '2022-03-10'
slug: []
categories:
  - R
tags:
  - Communication
image:
  caption: ''
  focal_point: ''
---


Can't argue with this 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">One thing I have learned about communication at work, and even more so when the work is remote is that, in order to be effective, you will often need to explain the same thing multiple times to different people, and sometimes even to the same people, but in a different way.</p>&mdash; Vicki (@vboykis) <a href="https://twitter.com/vboykis/status/1501851689841827853?ref_src=twsrc%5Etfw">March 10, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 