---
title: A Very Cool Dashboard
author: Kevin
date: '2019-01-19'
slug: a-very-cool-dashboard
categories:
  - R
tags: [Rstudio conference]
header:
  caption: ''
  image: ''
---


This [Dashboard](http://apps.garrickadenbuie.com/rstudioconf-2019/) is pretty awesome. Can't wait for all the videos to be posted. 
