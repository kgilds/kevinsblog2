---
title: Counting Goats
author: Kevin
date: '2022-01-14'
slug: []
categories:
  - R
tags:
  - web
image:
  caption: ''
  focal_point: ''
---


This an informative [post](https://www.rostrum.blog/2020/09/16/goatcounter-blogdown/) on using the web analytics site [GoatCounter](https://www.goatcounter.com/). It is easy to implement if you use [Netlify](netlify.com) and blogdown site. 