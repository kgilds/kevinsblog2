---
title: Craftmanship
author: Kevin
date: '2022-06-08'
slug: []
categories:
  - craftsmanship
tags: [craftmanship]
image:
  caption: ''
  focal_point: ''
---

I finished reading *The Molecule of More* today and last chapter caught my attention. Creativity is generated when there is a mix of dopamine and H&N chemical releases in the brain.

> Woodworking, knitting, painting, decorating, sewing....require the brain and hands to work together to create something new.

I don't disagree with this but I would argue coding is on that spectrum. I suspect the authors were trying to encourage folks to be content creators rather than consumers.
