---
title: Documenting Datasets
author: Kevin
date: '2019-11-27'
slug: documenting-datasets
categories:
  - R
tags:
  - R-Package
image:
  caption: ''
  focal_point: ''
---

I discovered a tip at the latest R-User Meetup to document datasets for a package. I was manually documenting datasets and not doing a great job.


The last couple of days; I have been using the File -> New -> R Documentation and this provides a template for more detailed documentation and will at least provide a detailed listing of variable data types. 

This [link](https://support.rstudio.com/hc/en-us/articles/200532317-Writing-Package-Documentation) provides more information about this feature. 

Updated[2019-12-01]

To use from the console use
```
promptData
```
for datasets or  ```prompt``` for functions. 

One thing I noted using this with a function is that the template did not export to Namespace. I had to manually enter export with the function. 

```
#' @export
fun_name <- function(){
sum(2 +2)
}
```