---
title: Useful Links
author: Kevin
date: '2020-08-09'
slug: useful-links
categories:
  - R
tags:
  - purrr
  - pwalk
  - parameterized reports
image:
  caption: ''
  focal_point: ''
---

I have been referencing this [article](https://medium.com/@urban_institute/iterated-fact-sheets-with-r-markdown-d685eb4eafce) to employ parameterized reports in R markdown. My only struggle has been employing the pwalk script to work. The script correctly renders for one council and null for the others.

Here is the Rstudio reference material on parameterized reports 


This [stackoverflow post](https://stackoverflow.com/questions/45156685/naming-iterative-rmarkdown-documents) makes it seem like there is a file argument for the pwalk function. I scoured the internet looking for another example of pwalk using this argument and I did not find it but I found this............

This [post](https://www.rostrum.blog/2020/03/12/knit-with-params/) is a dual purpose the purr template and example of using the xarigan package to produce a pdf of the slide. Here is [git-repo](https://github.com/matt-dray/ninja-knitting/blob/master/src/00_run.R). The 00_run.R file gave me the example I needed to employ. 