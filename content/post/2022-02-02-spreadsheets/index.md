---
title: Spreadsheets
author: Kevin
date: '2022-02-02'
slug: []
categories:
  - R
tags:
  - spreadsheets
image:
  caption: ''
  focal_point: ''
---

Spreadsheets-the good, the bad, and ugly. I can't wish them away but simply offer suggestions on how to work with them better. 

This article is a good starting point for topic of spreadsheets--that is if you must use a spreadsheet do it this way.
https://github.com/kbroman/Paper_DataOrg/blob/master/manuscript.pdf



I have a story/example for each element/point

* Be consistent
* Choose good names for things--
* Dates as YYYY-MM-DD
* No empty cells
* Put just one thing in a cell
* Make it rectangle--
* Create a data dictionary
* No calculations in the raw data
* No font colors or highlighting as data. 
* Make backups
* Use Data validation--using drop downs let one be consistent. 
* Save the data in plain text. 






