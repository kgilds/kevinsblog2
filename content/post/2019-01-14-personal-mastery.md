---
title: Personal Mastery
author: Kevin
date: '2019-01-14'
slug: personal-mastery
categories:
  - craftsmanship
tags:
  - Books
header:
  caption: ''
  image: ''
---

I have been trying to post everyday; last night I failed however the important point is that I am here tonight. I had a brief thought that since I am starting a two-week sprint on a report that I should take a hiatus. However, what if I doubled down and tried to write even better blog post.

I like the second approach better. Yesterday, I unearthed *The Fifth Discipline* and started reading the section on Personal Mastery. What a motivation and a needed read. 

The ability to hold creative tension. Tension occurs when vision and reality do not align. However the tension gives one energy to try and improve the reality. We tend to lower our standards rather than deal with the tension of not reaching the idea state. I was trying to lower my standards to ease my guilt for not blogging. However, I want to write and teach people to use data.   
