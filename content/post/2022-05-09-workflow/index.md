---
title: Workflow
author: Kevin
date: '2022-05-09'
slug: []
categories:
  - R
tags:
  - quarto
image:
  caption: ''
  focal_point: ''
---


Good workflow link to article with quarto 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">R Workflow article much improved with automatic Quarto tabs, variable recoding examples, more longitudinal data manipulation examples, creating a pop-up window data dictionary to guide analysis coding <a href="https://twitter.com/hashtag/Statistics?src=hash&amp;ref_src=twsrc%5Etfw">#Statistics</a> <a href="https://t.co/AWWpOz2nyW">https://t.co/AWWpOz2nyW</a> <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> <a href="https://twitter.com/vandy_biostat?ref_src=twsrc%5Etfw">@vandy_biostat</a> <a href="https://twitter.com/VUDataScience?ref_src=twsrc%5Etfw">@VUDataScience</a></p>&mdash; Frank Harrell (@f2harrell) <a href="https://twitter.com/f2harrell/status/1522607766141034497?ref_src=twsrc%5Etfw">May 6, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 