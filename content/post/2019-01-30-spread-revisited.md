---
title: Spread revisited
author: Kevin
date: '2019-01-30'
slug: spread-revisited
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

> 5:05-6:00 am

I tried to bind the data with bind_col but that did not work. I revisited the spread function and magically understood how to use implement with my data set.

I managed to start working on the dashboard and determined that this would be a bit of an overhaul of existing code and determined to make a new branch. First half rendered but it is rendering with 22.00 rather than 22.

