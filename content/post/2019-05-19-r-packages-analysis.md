---
title: R-Packages-Analysis
author: Kevin
date: '2019-05-19'
slug: r-packages-analysis
categories:
  - R
tags:
  - R-Package
image:
  caption: ''
  focal_point: ''
---

My interest is making my analysis project into a R-package and I have made almost all of my processing into functions but not all.  

My question is how to integrate regular rscripts into the package if you can't make everything a function. 

My question is indirectly address by the tutorial and Github Repository from Dave Kleinschmidt

[Tutorial](https://www.davekleinschmidt.com/r-packages/)
[Github Repo](https://github.com/kleinschmidt/r-packages)