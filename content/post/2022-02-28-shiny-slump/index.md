---
title: Shiny Slump
author: Kevin
date: '2022-02-28'
slug: []
categories:
  - R
tags:
  - Shiny
image:
  caption: ''
  focal_point: ''
---

I am in a bit of a Shiny slump as I have not built an application in weeks. I am trying to find a good dataset to use. 


A couple of ideas:

* Florida Transportation Disadvantaged Board

* TidyTuesday datasets. 

* CDC data

* Hud data
