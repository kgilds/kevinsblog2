---
title: An analysis as a package
author: Kevin
date: '2019-05-05'
slug: an-analysis-as-a-package
categories:
  - R
tags: [R-Package]
image:
  caption: ''
  focal_point: ''
---


I like this [post](https://www.r-bloggers.com/creating-an-analysis-as-a-package-and-vignette/) as a guide to build a package out of an analysis. Here is the [Git Repo](https://github.com/rmflight/ismbTweetAnalysis).

Ironically, the analysis I want to with this is very complex; hence it could benefit from a package and secondly it will be very challenging to do. Honestly, I am not sure if I can start midway--may have to a a fresh start so to speak. 