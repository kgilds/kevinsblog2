---
title: Learning to Code
author: Kevin
date: '2022-06-03'
slug: []
categories:
  - R
tags:
  - code
  - python
  - wic
image:
  caption: ''
  focal_point: ''
---

This tweet made me think about my experience:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">What would it look like to live in a world where we took &quot;teaching people to code&quot; seriously? It would look like people caring that many high schools don&#39;t have coding classes (&amp; it&#39;s systematically distributed) and access to majors in CS is essentially steered by HS code access.</p>&mdash; Dr. Cat Hicks 📈🦄🏳️‍🌈 (@grimalkina) <a href="https://twitter.com/grimalkina/status/1532763072208572417?ref_src=twsrc%5Etfw">June 3, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 



I learned to code because I was annoyed by a concrete problems that made my life difficult. Although, I am not a Python person, my favorite intro to coding is [_Python for Everyone_](https://www.py4e.com/). It helps the learner use code and data to solve problems. The books provides learners foundations to write a script to solve a problem. That is what I am hoping to do. 


