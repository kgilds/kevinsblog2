---
title: Bullet Tables
author: Kevin
date: '2022-04-18'
slug: []
categories:
  - R
tags:
  - bullet charts
  - gtExtras
image:
  caption: ''
  focal_point: ''
---


I have used bullet charts in the past but I found myself moving back to using tables because I wanted to be sure the audience understood the data. Graphs are great for trends but the data I was displaying required the reader to make precise comparisons. I think I found a happy spot with this [post](https://jthomasmock.github.io/gtExtras/articles/plots-in-gt.html) from Thomas Mock.   