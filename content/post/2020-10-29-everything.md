---
title: Everything
author: Kevin
date: '2020-10-29'
slug: everything
categories:
  - R
tags: [dplyr]
image:
  caption: ''
  focal_point: ''
---

I needed to do a quick analysis and needed to drop many columns but needed to rearrange other columns. Low and behold the ```everything ()```function from the dplyr package.



[Here](https://suzan.rbind.io/2018/01/dplyr-tutorial-1/#re-ordering-columns) is helpful example.