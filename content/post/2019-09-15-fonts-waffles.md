---
title: Fonts & Waffles
author: Kevin
date: '2019-09-15'
slug: fonts-waffles
categories:
  - R
tags:
  - waffle
image:
  caption: ''
  focal_point: ''
---

I want to display proportions in a fancy way and the package to do that is the [waffle package](https://rud.is/b/2019/07/12/quick-hit-waffle-1-0-font-awesome-5-pictograms-and-more/)

This [stackoverflow post](https://stackoverflow.com/questions/36924251/import-font-into-r-using-extrafont-package) helped me figure out how to import font-awesome fonts into R. I kept getting a blank response from the ~extrafont:: import_fonts()~ function. Needed to point the function to a directory where the files were located. 