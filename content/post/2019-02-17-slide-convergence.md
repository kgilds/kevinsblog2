---
title: Slide Convergence
author: Kevin
date: '2019-02-17'
slug: slide-convergence
categories:
  - R
  - DataCamp
tags:
  - WorkNotes
  
header:
  caption: ''
  image: ''
  
---


> 4:30 am - 6:50 am

The flow for the slides finally converged on me this morning and it was great being both creative and productive. I learned a setting in ioslides to make the slide wider--Now, I can put the dashboard in the i-frame and it works well. This [guide](https://bookdown.org/yihui/rmarkdown/ioslides-presentation.html#advanced-layout) My only trouble is with the behavior report. It renders fine on its own but I am not able to source the file without error. 


Today, I completed the first section of Modeling with data in the Tidyverse on Data Camp. I enjoyed and it may be worth paying to complete the rest. 