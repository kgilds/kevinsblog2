---
title: Quarto
author: Kevin
date: '2022-01-20'
slug: []
categories:
  - Open Source
tags:
  - writing
image:
  caption: ''
  focal_point: ''
---


I saw this tonight on twitter

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">My bet on quarto <a href="https://t.co/XNBNgTwMTh">https://t.co/XNBNgTwMTh</a></p>&mdash; Romain Lesur (@RLesur) <a href="https://twitter.com/RLesur/status/1484329258637955074?ref_src=twsrc%5Etfw">January 21, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

This seems like fun, and you can use within [RStudio](https://quarto.org/docs/projects/code-execution.html)

