---
title: ggplot examples
author: Kevin
date: '2020-01-03'
slug: ggplot-examples
categories:
  - R
tags: [ggplot2]
image:
  caption: ''
  focal_point: ''
---

I have been stuck on how to polish up a graph and I came across this [site](https://cedricscherer.netlify.com/2019/08/05/a-ggplot2-tutorial-for-beautiful-plotting-in-r/). 

I am anxious to try the scales free argument. There is also a useful section on developing your own theme. 