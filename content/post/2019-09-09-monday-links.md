---
title: Monday Links
author: Kevin
date: '2019-09-09'
slug: monday-links
categories:
  - R
tags:
  - R-Package
image:
  caption: ''
  focal_point: ''
---

I am going to use this function:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">TIL about <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> stringr::str_squish()! <br><br>str_trim() just gets rid of whitespace at the beginning and end of a string, but str_squish() does that *and* reduces repeated whitespace within the string, too! <br><br>thanks <a href="https://twitter.com/_sharleen_w?ref_src=twsrc%5Etfw">@_sharleen_w</a> for the tip 🙏🙏 <a href="https://t.co/1sATJaaEJi">pic.twitter.com/1sATJaaEJi</a></p>&mdash; Sharla Gelfand (@sharlagelfand) <a href="https://twitter.com/sharlagelfand/status/1171064192939560960?ref_src=twsrc%5Etfw">September 9, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


### Public Administration and R!

[Program Evaluation for Public Service Course Site](https://evalf19.classes.andrewheiss.com/syllabus/)

*Impact Evaluation*(https://openknowledge.worldbank.org/bitstream/handle/10986/25030/9781464807794.pdf?sequence=2&isAllowed=y)