---
title: Excel & Glue
author: Kevin
date: '2022-05-14'
slug: []
categories:
  - R
tags:
  - Glue
  - openxlsx
image:
  caption: ''
  focal_point: ''
---


I have been working on a script to fill in values of an Excel Spreadsheet (long story) and dear reader if I pull this off you will  be reading more about this. Tha main package to do this with is [`openxlsx`]

Adding values to a spreadsheet is clear but adding a value replaces the current content in the cell. In this case, we want to keep the label in the spreadsheet. 


I checked out the [`glue pacakge`](https://glue.tidyverse.org/)

When I introduced a `glue` call to keep the label--the variable value moved down a cell.

> When something is not working as expected; check the data type. 

I changed the type back to character in a pipeline and it works.


```r

name_1 <- "John Doe"

name_1_label <- glue('Student Name {name_1}') %>%
as.character()
openxlsx::writeData(global, "Summary",  x = name_1_label, startCol = 2, startRow = 2)
```

This will place the "Student Name: John Doe" in cell B2 of the Excel Template. 

