---
title: Getting Things Done
author: Kevin
date: '2022-06-06'
slug: []
categories:
  - R
tags:
  - management
image:
  caption: ''
  focal_point: ''
---

🧐

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I have a theory that people who deride quirks of <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> don’t know how useful those things are. R is full of things that are clearly written FOR people who are not software engineers but who need &amp; want to code and most importantly need to get shit done iteratively &amp; efficiently <a href="https://t.co/LTZ48J4DTX">https://t.co/LTZ48J4DTX</a></p>&mdash; Adam Gruclear Submarine (@AdamGruer) <a href="https://twitter.com/AdamGruer/status/1533801161584967680?ref_src=twsrc%5Etfw">June 6, 2022</a></blockquote>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
