---
title: Move Columns Around
author: Kevin
date: '2022-07-06'
slug: []
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---


Sometimes, you don't want to move columns around with dplyr. The `gt::cols_move` function from the [gt](https://gt.rstudio.com/) package is helpful here


```r
images_tbl_1 <- images %>%
  gt() %>%
  cols_label(
    "FALSE" = "In Progress",
    "TRUE" = "Success") %>% 
  gt_img_rows(columns = link, height = 25) %>%
  gt_merge_stack(col1 = link, col2 = outcome) %>%
  cols_label(
    link = "Outcome"
  ) %>%
  cols_move(
    columns = c("FALSE", "TRUE"),
    after = link
  ) %>% 
  

    gt_theme_538()
```
Sorry can't share the output!
