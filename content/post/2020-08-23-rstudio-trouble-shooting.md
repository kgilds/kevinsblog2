---
title: RStudio Trouble Shooting
author: Kevin
date: '2020-08-23'
slug: rstudio-trouble-shooting
categories:
  - R
tags: []
image:
  caption: ''
  focal_point: ''
---

Last week I ran into an abrupt issue in RStudio of not being able to create new files or save files  I checked another project and did not experience the same issue.My initial solution was to update R and this worked temporarily.

I googled the error I was having and came across [this iussue on github](https://github.com/rstudio/rstudio/issues/2037)

My operating system is Ubuntu 18.04 so tried the solution provided in the comments.

In the terminal type:


`cd "mycool project ` and type `sudo chown -R $(whoami) .Rproj.user`

The [Chown command](https://linuxize.com/post/linux-chown-command/) in linux allows one to change the user and or group ownership of a directory

