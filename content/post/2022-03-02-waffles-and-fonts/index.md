---
title: Waffles and Fonts
author: Kevin
date: '2022-03-02'
slug: []
categories:
  - R
tags: [galt, extrafont]
image:
  caption: ''
  focal_point: ''
---

I seem to have lost fonts when I had to complete a partial re-install of my operating system. My waffle plots were not working, and I had to refresh my memory of how to fix the situation.  

This [post](https://www.listendata.com/2019/06/create-infographics-with-r.html) directed me towards the fonts that I needed to install. I also needed the extrafont package to register them with R. 