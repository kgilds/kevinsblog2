---
title: News from Github
author: Kevin
date: '2019-01-08'
slug: news-from-github
categories:
  - R
tags:
  - Git
header:
  caption: ''
  image: ''
---

Github is now allowing one to create private repos with up to three collaborators. 
Here is the blog [post](https://blog.github.com/2019-01-07-new-year-new-github/) 


