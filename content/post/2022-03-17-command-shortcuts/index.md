---
title: command shortcuts
author: Kevin
date: '2022-03-17'
slug: []
categories:
  - R
tags:
  - tips
image:
  caption: ''
  focal_point: ''
---

so useful........ ctrl-shift-c to comment off lines of code and to add them back. 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Ctrl-Shift-C to comment out selected <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> code , Ctrl-Shift-C to uncomment again. So much better than commenting out every line one by one, and deleting the # one by one. I forgot who introduced me to this wisdom, but thank you!</p>&mdash; Martine Jansen (@nnie_nl) <a href="https://twitter.com/nnie_nl/status/1503002606079291397?ref_src=twsrc%5Etfw">March 13, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

