---
title: Link to file in Academic Theme
author: kevin
date: '2020-05-31'
slug: link-to-file-in-academic-them
categories:
  - R
tags:
  - blogdown
image:
  caption: ''
  focal_point: ''
---


This [post](https://lmyint.github.io/post/hugo-academic-tips/#adding-cv) is great and thankful it pointed out how to link to a file with the Academic Theme. Prior I had been linking to a google drive document. 