---
title: Themes
author: Kevin
date: '2022-06-18'
slug: []
categories:
  - WIC
tags: []
image:
  caption: ''
  focal_point: ''
---

Working on items to focus on when writing

* Communication with people and machines.
  * Virtuous Cycle: Learning to code helps you with spreadsheets and other computer literacy.
* Tidy data
* Checklist or perspective when coding may be more beneficial. 