---
title: Shiny Server Options
author: Kevin
date: '2019-06-20'
slug: shiny-server-options
categories:
  - R
tags:
  - Shiny
image:
  caption: ''
  focal_point: ''
---

Last weekend, I spun up a shiny-serve image on Digital Ocean. It was easy to use a pre-defined shiny server from [Simply Stats](https://cloud.digitalocean.com/marketplace/5ca53ade3b7d246b5b29b1f0?i=3f9293) on the digital ocean market place. It was easy to spin up but I am hesitant to dive. My hesitation that I don't want to put a  large investment into this droplet what I am really interested is below.


[Shiny Studio](https://hub.docker.com/r/dm3ll3n/shinystudio)

I think my approach would be create droplet on DO with this template 
[Docker Image](https://cloud.digitalocean.com/marketplace/5ba19751fc53b8179c7a0071?i=3f9293). I will need to think about it. 
