---
title: Talking with Machine
author: Kevin
date: '2022-04-12'
slug: []
categories:
  - R
tags: [communication]
image:
  caption: ''
  focal_point: ''
---


I said it out loud today while I was training a new staff person–not a data person. This spreadsheet was not designed for you; it was designed to be uploaded into a databases, turned into a useful report that can be understood by you; converted to another file to be uploaded into our accounting system.

The main function for a person to work with this spreadsheet is to make sure is in the right structure for upload and to convert to a text file. They are learning to communicate with machines.