---
title: A Delay
author: Kevin
date: '2022-02-14'
slug: []
categories:
  - R
tags: [linux]
image:
  caption: ''
  focal_point: ''
---

Yesterday, 2022-02-13, my operating system would not boot. I had to complete a refresh operating system. Thankfully, files kept in tact but had to re install R and other fun tools. I thought about using docker for developer environment but did not feel I wanted that stress of something new. 