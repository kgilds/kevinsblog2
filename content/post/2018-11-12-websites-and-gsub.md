---
title: Websites and Reports
author: Kevin
date: '2018-11-12'
slug: websites-and-gsub
categories:
  - R
  - worknotes
tags: [Rmarkdown]
header:
  caption: ''
  image: ''
---

I reviewed the Rmarkdown website and I am intrigued by building websites with Rmd files. The link is here 
(https://rmarkdown.rstudio.com/rmarkdown_websites.htm).


This may be a better idea than this [post](https://kgilds.rbind.io/post/data-science-podcast/). 


Speaking of Rmarkdown I am absolutely stuck on parameterized reports. My goal is to figure this out next year. 


