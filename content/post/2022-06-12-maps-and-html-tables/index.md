---
title: Maps and html tables
author: 'Kevin '
date: '2022-06-12'
slug: []
categories:
  - R
tags:
  - leaflet
  - maps
image:
  caption: ''
  focal_point: ''
---


I have been focused on htlml tables to mimic and Excel worksheep and maps.

This has been my favorite site to get me through: https://www.w3schools.com/html/html_tables.asp



Here is an interesting tweet of mapping in Florida

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Some interesting discussion by <a href="https://twitter.com/rick_pack2?ref_src=twsrc%5Etfw">@rick_pack2</a> over on <a href="https://twitter.com/StackOverflow?ref_src=twsrc%5Etfw">@StackOverflow</a> about how to use tidycensus + <a href="https://twitter.com/chrisprener?ref_src=twsrc%5Etfw">@chrisprener</a>&#39;s biscale to make bivariate choropleth maps in <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a>: <a href="https://t.co/QWEFKvQ6T9">https://t.co/QWEFKvQ6T9</a> <a href="https://t.co/ADUA6iUbWa">pic.twitter.com/ADUA6iUbWa</a></p>&mdash; Kyle Walker (@kyle_e_walker) <a href="https://twitter.com/kyle_e_walker/status/1536048579835576325?ref_src=twsrc%5Etfw">June 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 