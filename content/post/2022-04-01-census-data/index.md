---
title: Census Data
author: Kevin
date: '2022-04-01'
slug: []
categories:
  - R
tags: [census]
image:
  caption: ''
  focal_point: ''
---

I was in a meeting this week and it was reported my home county (Polk, Florida) was the 7th fastest growing country in the United States.

Here is a tool to check out

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">If you want to improve your mastery of {leaflet}, {flexdashboard}, and {shiny}, study the source code of <a href="https://twitter.com/kyle_e_walker?ref_src=twsrc%5Etfw">@kyle_e_walker</a>&#39;s &quot;Locating Neighborhood Diversity&quot; <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> <a href="https://twitter.com/hashtag/shiny?src=hash&amp;ref_src=twsrc%5Etfw">#shiny</a> app. A gem worth bookmarking for numerous reasons. <a href="https://t.co/SZgvOkkvAM">https://t.co/SZgvOkkvAM</a> <a href="https://t.co/Mqg7tduJxo">pic.twitter.com/Mqg7tduJxo</a></p>&mdash; David Ranzolin (@daranzolin) <a href="https://twitter.com/daranzolin/status/1510031957953970178?ref_src=twsrc%5Etfw">April 1, 2022</a></blockquote>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
