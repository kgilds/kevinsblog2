---
title: Report Writing
author: Kevin
date: '2019-01-23'
slug: report-writing
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

I submitted a report written in Word today. It started in scripts and then Rmarkdown.  A couple of things, I need to do prior to the next report and never synch between Word and Rmarkdown again. Find an example script to get PDF output to [double space](https://tex.stackexchange.com/questions/266160/knitr-double-spacing-in-an-html-document). I also need a better [spelling](https://github.com/ropensci/spelling) and [grammer](https://github.com/ropenscilabs/gramr) check. 



