---
title: Git Progression
author: Kevin
date: '2019-01-06'
slug: git-progression
categories:
  - R
tags:
  - chromebook data science
  - git
header:
  caption: ''
  image: ''
---

This is a great [post](https://suzan.rbind.io/2018/03/reflections-4-months-of-github/) on jumping into Git and using Github. 

> If you can code in R, you can code in Git's command line.--Suzann Baert

This is true but a prerequisite for me to truly learning git was finally learning how to mange files through the terminal and truly understanding absolute and relative paths. 

I needed the  *Organizing Data Science Projects* from the [Chromebook Data Science](http://jhudatascience.org/chromebookdatascience/index.html) for the structured practice. Here is a [post](https://kgilds.rbind.io/post/the-relative-path/) detailing my recent triumph over file paths. 

The post also highlights the video below.  My take away from this video is the git push -u command and merging branches from the command line. Wow, my workflow has been to push to the remote; merge from Gitlab  and then pull the changes down from the master. 



<iframe width="560" height="315" src="https://www.youtube.com/embed/HVsySz-h9r4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>














