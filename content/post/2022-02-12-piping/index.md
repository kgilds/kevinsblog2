---
title: Piping
author: Kevin
date: '2022-02-12'
slug: []
categories:
  - CLI
tags:
  - CLI
  - Command Line
image:
  caption: ''
  focal_point: ''
---

Today, while going through the lessons from [Data Science at the Command Line](https://datascienceatthecommandline.com/chapter-4-creating-command-line-tools.html) I learned how to pipe operations.

You use `|` operator. And when you use '|` and hit enter you will get a new line 

```
curl sl "website" |

> trim

```

And the way to save the script is to run the `fc` command and that will open the nano editor to save the file. 

Where I got confused was with setting the file permissions. 