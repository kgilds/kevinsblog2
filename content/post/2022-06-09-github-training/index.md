---
title: Github Training
author: Kevin
date: '2022-06-09'
slug: []
categories:
  - R
tags: [git, github]
image:
  caption: ''
  focal_point: ''
---


Training with Github:: https://github.blog/2022-06-06-introducing-github-skills/

This course is seems like it would be great for those just starting out. However, I can't help but think the training is good because I have the experience with Git and Github.  I am not sure how much I would grasp and retain without the experience. 