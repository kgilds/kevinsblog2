---
title: Don't Forget the NS
author: Kevin
date: '2019-09-28'
slug: don-t-forget-the-ns
categories:
  - R
tags:
  - Shiny
  - modules
image:
  caption: ''
  focal_point: ''
---

For the last couple of days I was struggling to connect a Shiny Input with a displayed data frame. I had a lot of silly mistakes prior and could not pin the error down. I deduced the error was a small detail I was missing and this turned out to be ture.

* My first silly mistake was not wrapping the tableOut in ns.

Below are examples from [Shiny Modules](https://shiny.rstudio.com/articles/modules.html)


```fluidRow(
    column(6, plotOutput(ns("plot1"), brush = ns("brush"))),
    column(6, plotOutput(ns("plot2"), brush = ns("brush")))
  )
```

* My second error was not wrapping the Shiny Input in ns-this I forgot. Plus, if you notice you need to wrap the id value in parens seperately; from the call. 



```tagList(
    fileInput(ns("file"), label),
    checkboxInput(ns("heading"), "Has heading"),
    selectInput(ns("quote"), "Quote", c(
      "None" = "",
      "Double quote" = "\"",
      "Single quote" = "'"
    ))
  )
}
```

