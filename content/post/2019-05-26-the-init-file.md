---
title: The  Init File
author: Kevin
date: '2019-05-26'
slug: the-init-file
categories:
  - Org-mode
  - e-macs
tags:
  - org-mode
  - e-macs
image:
  caption: ''
  focal_point: ''
---

I spent my Saturday obsessing my the Init file within e-macs. My main purpose is to set up a capture template.  It is really this easy as selecting C-x C-f.  

`C-x C-f ~/.emacs.`

Here is a [post on 5h3 matter](https://www.gnu.org/software/emacs/manual/html_node/efaq-w32/Location-of-init-file.html)