---
title: More Docker Volumes
author: Kevin
date: '2022-02-07'
slug: []
categories:
  - Docker
tags:
  - CLI
image:
  caption: ''
  focal_point: ''
---

Today, while going through the lessons from [Data Science at the Command Line](https://datascienceatthecommandline.com/2e/chapter-2-getting-started.html) I noticed after I exited the container the files I created were in my local directory. I know this is supposed to happen but to experience it is to learn it.  
