---
title: More Quarto
author: Kevin
date: '2022-04-04'
slug: []
categories:
  - R
tags:
  - quarto
image:
  caption: ''
  focal_point: ''
---


I have been working in Quarto for about a week now and liking it more and more.

This [post](https://www.apreshill.com/blog/2022-04-we-dont-talk-about-quarto/) from Alison Hill has some great reference points