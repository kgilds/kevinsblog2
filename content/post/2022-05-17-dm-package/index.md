---
title: DM Package
author: Kevin
date: '2022-05-17'
slug: []
categories:
  - R
tags: [dm]
image:
  caption: ''
  focal_point: ''
---

This R package looks interesting and useful.
https://cynkra.github.io/dm/ and this article here talks about turning data frames into tables. 
https://cynkra.github.io/dm/articles/howto-dm-df.html

