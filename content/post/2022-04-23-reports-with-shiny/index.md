---
title: Reports with Shiny
author: Kevin
date: '2022-04-23'
slug: []
categories:
  - R
tags:
  - Shiny
  - Rmarkdown
image:
  caption: ''
  focal_point: ''
---

I have been making use of the feature to download reports in a Shiny Application.

The caveat to this approach is that if your Rmarkdown report is complex with a lot of different `R` packages---your application is going to be taking on more dependencies. I finally hit my limit with one series of reports that just had too many dependencies.
