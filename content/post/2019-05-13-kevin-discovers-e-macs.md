---
title: Kevin discovers e-macs
author: Kevin
date: '2019-05-13'
slug: kevin-discovers-e-macs
categories:
  - Open Source
tags:
  - org
  - emacs
image:
  caption: ''
  focal_point: ''
---

I fell into an `e-macs` rabbit hole today; wound up downloading to my Windows machine. Mainly, I became intrigued because of org.files and organizing projects and being able to Clock tasks within a project. 

Here are some helpful links:

[Tutorial](https://orgmode.org/worg/org-tutorials/orgtutorial_dto.html)

[Medium Post](https://medium.com/@mgkennard/lets-get-going-with-org-mode-b1ff648bcbc3)


On making a [calendar.org file](https://errickson.net/org_agenda_calendar.html)

