---
title: Data Validation
author: Kevin
date: '2022-04-21'
slug: []
categories:
  - R
tags:
  - testing
  - pointblank
image:
  caption: ''
  focal_point: ''
---


I saw this post on a twitter thread about [`pointblank`](https://rich-iannone.github.io/pointblank/) package.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">There are many alternatives. This is a great overview of options: <a href="https://t.co/UEoO6uDLZJ">https://t.co/UEoO6uDLZJ</a></p>&mdash; Crystal Lewis (@Cghlewis) <a href="https://twitter.com/Cghlewis/status/1516879837943713792?ref_src=twsrc%5Etfw">April 20, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

I was able to watch a few minutes of the presentation and liked it. 