---
title: Data Types
author: Kevin
date: '2020-11-30'
slug: data-types
categories:
  - R
tags:
  - dplyr
image:
  caption: ''
  focal_point: ''
---


I need to look how to change a data type on a regular basis.

Here is an example form of one particular column. 

```r
df %>%
dplyr::mutate_at(vars(site), as.character)%>%
```

If I find the script for multiple columns, I will add. 