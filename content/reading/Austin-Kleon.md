+++
# Project title.
title = "Austin Kleon"

# Date this page was created.
date = 2019-03-30T00:00:00

# Project summary to display on homepage.
summary = "A wonderful blog on the intersection of art and life"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Art"]

# Optional external URL for projroject detail page).
external_link = "https://austinkleon.com/"

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = "#"

# Links (optional).
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"

url_slides = ""
url_video = ""
url_code = ""

+++