+++
# Project title.
title = "Seth Godin"

# Date this page was created.
date = 2019-03-30T00:00:00


# Project summary to display on homepage.
summary = "Leadership & encouragement"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Leadership"]

# Optional external URL for projroject detail page).
external_link = "https://www.sethgodin.com/"

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = "#"

# Links (optional).
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"

url_slides = ""
url_video = ""
url_code = ""
url_github = "https://github.com/r4ds/bookclub-rpkgs"

+++