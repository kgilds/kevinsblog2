+++
# Project title.
title = "R-Weekly"

# Date this page was created.
date = 2019-03-30T00:00:00

# Project summary to display on homepage.
summary = "R-weekly aggregates R content."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["R"]

# Optional external URL for project (replaces project detail page).
external_link = "https://rweekly.org/"

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references 
#   `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides = "#"

# Links (optional).
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"
#url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"

url_slides = ""
url_video = ""
url_code = ""

+++